#version 460 core

out vec2 fragmentTextureCoordinate;

uniform mat4 orthographicMatrix;
uniform vec2 vertexPositions[4];

const vec2 UVs[4] = vec2[4](vec2(0, 0), vec2(1, 0), vec2(1, 1), vec2(0, 1));

void main() {

    gl_Position = orthographicMatrix * vec4(vertexPositions[gl_VertexID], 0.0, 1.0);
    fragmentTextureCoordinate = UVs[gl_VertexID];
}  