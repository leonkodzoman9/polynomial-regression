#include "Buffer.h"



Buffer::Buffer() : ID(0), target(0) {}
Buffer::Buffer(uint32_t target) : target(target) {

    glGenBuffers(1, &this->ID);
    glBindBuffer(this->target, this->ID);
}
Buffer::~Buffer() {

    glDeleteBuffers(1, &this->ID);
}

Buffer::Buffer(Buffer&& rhs) noexcept {

    if (this != &rhs) {

        this->ID = std::exchange(rhs.ID, 0);
        this->target = std::exchange(rhs.target, 0);
    }
}
Buffer& Buffer::operator=(Buffer&& rhs) noexcept {

    if (this != &rhs) {

        glDeleteBuffers(1, &this->ID);

        this->ID = std::exchange(rhs.ID, 0);
        this->target = std::exchange(rhs.target, 0);
    }

    return *this;
}
