#pragma once

#include "Includes.h"

#include "Vector.h"



class Window {

private:

	std::string title;

	GLFWwindow* window;

	Window();
	~Window();

	Window(const Window& rhs) = delete;
	Window& operator=(const Window& rhs) = delete;
	Window(Window&& rhs) noexcept = delete;
	Window& operator=(Window&& rhs) noexcept = delete;

public:

	Vector2i size;

	static Window& getInstance();

	void initialize(Vector2i windowSize, const std::string& windowName = "Window");

	GLFWwindow* getWindow();

	std::string getTitle() const;
	void setTitle(const std::string& windowName);

	void setMouseState(uint32_t state);
};





