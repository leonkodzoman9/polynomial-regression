#include "Mouse.h"

#include "Vector.h"
#include "Window.h"



double globalYOffset = 0;

void scrollCallback(GLFWwindow* window, double xOffset, double yOffset) {

	globalYOffset = yOffset;
}



Mouse::Mouse() {

	this->reset();

	this->scrollAmount = 0;
	this->currentTime = 0;

	glfwSetScrollCallback(Window::getInstance().getWindow(), scrollCallback);
};

Mouse& Mouse::getInstance() {

	static Mouse instance;

	return instance;
}

void Mouse::updatePressTransitionCounter(uint32_t button, float currentTime) {

	this->pressTransitionCounter[button].first = this->pressTransitionCounter[button].second;
	this->pressTransitionCounter[button].second = currentTime;
}
void Mouse::updateReleaseTransitionCounter(uint32_t button, float currentTime) {

	this->releaseTransitionCounter[button].first = this->releaseTransitionCounter[button].second;
	this->releaseTransitionCounter[button].second = currentTime;
}

void Mouse::update() {

	GLFWwindow* window = Window::getInstance().getWindow();

	this->currentTime = std::chrono::duration_cast<std::chrono::nanoseconds>(std::chrono::steady_clock::now().time_since_epoch()).count() / 1'000'000.0f;

	this->scrollAmount = globalYOffset;
	globalYOffset = 0;

	for (int i = 0; i <= GLFW_MOUSE_BUTTON_LAST; i++) {

		this->previousState[i] = this->currentState[i];
		this->currentState[i] = glfwGetMouseButton(window, GLFW_MOUSE_BUTTON_1 + i) == GLFW_PRESS;

		bool currentState = this->currentState[i];
		bool previousState = this->previousState[i];

		if (!previousState && currentState) {
			this->updatePressTransitionCounter(i, currentTime);
		}

		if (previousState && !currentState) {
			this->updateReleaseTransitionCounter(i, currentTime);
		}
	}

	this->previousPosition = this->currentPosition;
	glfwGetCursorPos(Window::getInstance().getWindow(), &this->currentPosition.x, &this->currentPosition.y);
}
void Mouse::reset() {

	std::fill(this->currentState.begin(), this->currentState.end(), 0);
	std::fill(this->previousState.begin(), this->previousState.end(), 0);

	this->currentTime = 0;
	std::fill(this->pressTransitionCounter.begin(), this->pressTransitionCounter.end(), std::pair<float, float>(0, 0));
	std::fill(this->releaseTransitionCounter.begin(), this->releaseTransitionCounter.end(), std::pair<float, float>(0, 0));
}

Vector2d Mouse::position() const {

	return this->currentPosition;
}
Vector2d Mouse::deltaPosition() const {

	return this->currentPosition - this->previousPosition;
}

double Mouse::scroll() const {

	return this->scrollAmount;
}

bool Mouse::held(uint32_t button) const {

	return this->currentState[button];
}
bool Mouse::pressed(uint32_t button) const {

	return this->currentState[button] && !this->previousState[button];
}
bool Mouse::released(uint32_t button) const {

	return !this->currentState[button] && this->previousState[button];
}

float Mouse::heldFor(uint32_t button) const {

	return this->held(button) ? this->currentTime - this->pressTransitionCounter[button].second : 0;
}
float Mouse::releasedFor(uint32_t button) const {

	return this->released(button) ? this->currentTime - this->releaseTransitionCounter[button].second : 0;
}

float Mouse::pressDelta(uint32_t button) const {

	return this->pressTransitionCounter[button].second - this->pressTransitionCounter[button].first;
}
float Mouse::releaseDelta(uint32_t button) const {

	return this->releaseTransitionCounter[button].second - this->releaseTransitionCounter[button].first;
}



