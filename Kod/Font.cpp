#include "Font.h"

#include <ft2build.h>
#include FT_FREETYPE_H  




Font::Font() {

    this->height = 0;
}
Font::Font(Font&& rhs) noexcept {

    this->height = rhs.height;
    for (int i = 0; i < 256; i++) {
        this->symbols[i] = std::move(rhs.symbols[i]);
    }
}
Font& Font::operator=(Font&& rhs) noexcept {

    this->height = rhs.height;

    if (this != &rhs) {

        for (int i = 0; i < 256; i++) {
            this->symbols[i] = std::move(rhs.symbols[i]);
        }
    }

    return *this;
}
Font::Font(std::string fontName, int height) {


    this->height = height;

    FT_Library library;
    if (FT_Init_FreeType(&library)) {
        printf("Could not initialize FreeType.\n");
        throw;
    }

    FT_Face face;
    if (FT_New_Face(library, ("C:/Windows/Fonts/" + fontName + ".ttf").data(), 0, &face)) {
        printf("Could not load font.\n");
        throw;
    }

    FT_Set_Pixel_Sizes(face, 0, height);

    glPixelStorei(GL_UNPACK_ALIGNMENT, 1); 

    for (int i = 0; i < 256; i++) {

        if (FT_Load_Char(face, i, FT_LOAD_RENDER)) {
            continue;
        }

        Texture symbol(GL_TEXTURE_2D);
        symbol.setData(GL_TEXTURE_2D, GL_RED, face->glyph->bitmap.width, face->glyph->bitmap.rows, GL_RED, GL_UNSIGNED_BYTE, face->glyph->bitmap.buffer);
        symbol.setParameter(GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
        symbol.setParameter(GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
        symbol.setParameter(GL_TEXTURE_MIN_FILTER, GL_LINEAR);
        symbol.setParameter(GL_TEXTURE_MAG_FILTER, GL_LINEAR);

        this->symbols[i].texture = std::move(symbol);
        this->symbols[i].size = Vector2f((float)face->glyph->bitmap.width, (float)face->glyph->bitmap.rows);
        this->symbols[i].offset = Vector2f((float)face->glyph->bitmap_left, (float)face->glyph->bitmap_top);
        this->symbols[i].characterWidth = face->glyph->advance.x / 64.0f;
    }

    glPixelStorei(GL_UNPACK_ALIGNMENT, 4);

    FT_Done_Face(face);
    FT_Done_FreeType(library);
}

int Font::getHeight() const {

    return this->height;
}
Symbol& Font::getSymbol(uint8_t index) {

    return this->symbols[index];
}