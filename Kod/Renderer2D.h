 #pragma once

#include "Includes.h"

#include "Vector.h"
#include "Matrix.h"
#include "ShaderProgram.h"
#include "BufferDescriptor.h"



class Font;



enum TEXT_ALIGN { TOPLEFT, MIDTOP, TOPRIGHT, MIDLEFT, CENTER, MIDRIGHT, BOTTOMLEFT, MIDBOTTOM, BOTTOMRIGHT };

static float TextOffsetX[9] = { 0, -0.5, -1, 0, -0.5, -1, 0, -0.5, -1 };
static float TextOffsetY[9] = { 1, 1, 1, 0.5, 0.5, 0.5, 0, 0, 0 };

class Renderer2D {

private:

    // Defines the window
    Vector4f viewBoundary;
    Matrix4f orthographicMatrix;

    // All shaders
    ShaderProgram shaderLine;
    ShaderProgram shaderEllipse;
    ShaderProgram shaderTriangle;
    ShaderProgram shaderRectangle;
    ShaderProgram shaderFont;

    // Empty descriptor just because it has to be that way sometimes
    BufferDescriptor descriptorEmpty;

    Renderer2D();

    Renderer2D(const Renderer2D& rhs) = delete;
    Renderer2D operator=(const Renderer2D& rhs) = delete;
    Renderer2D(Renderer2D&& rhs) noexcept = delete;
    Renderer2D operator=(Renderer2D&& rhs) noexcept = delete;

    // Update the orthographic matrix based on the view boundary
    void updateOrthographicMatrix();

public:

    static Renderer2D& getInstance();

    // Get posistion of vector from screen coordinates to world coordinates
    Vector2f getPositionInWorldSpace(Vector2f screenSpace);
    // Get posistion of vector from world coordinates to screen coordinates
    Vector2f getPositionInScreenSpace(Vector2f worldSpace);

    // Move viewport
    void moveViewport(Vector2f direction);
    // Zoom viewport to a given screen space point
    void zoomViewport(Vector2f zoomPoint, float zoomMultiplier);

    // Draw point line between two points with a color and width
    void drawPointLine(Vector2f point1, Vector2f point2, Vector4f color, float width = 1);
    // Draw line from a point at an angle 
    void drawAngleLine(Vector2f point, float angle, float length, Vector4f color, float width = 1);

    // Draw ellipse at given point with radii and colors
    void drawEllipse(Vector2f point, Vector2f radii, Vector4f outlineColor, Vector4f fillColor, float width = 1, float angle = 0, int vertexCount = 32);
    // Draw circle at given point with radii and colors
    void drawCircle(Vector2f point, float radius, Vector4f outlineColor, Vector4f fillColor, float width = 1, int vertexCount = 32);

    // Draw a single triangle defined by 3 points
    void drawTriangle(Vector2f point1, Vector2f point2, Vector2f point3, Vector4f outlineColor, Vector4f fillColor, int width = 1);
    // Draw rectangle between points
    void drawRectangle(Vector2f center, Vector2f size, Vector4f outlineColor, Vector4f fillColor, int width = 1, float angle = 0);

    // Draw text on screen
    void drawText(Font& font, std::string text, Vector2f anchorPoint, float height, Vector4f color, TEXT_ALIGN textAlign = TEXT_ALIGN::CENTER);
};












