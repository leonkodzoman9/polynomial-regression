#include "Timer.h"



Timer::Timer() {

    this->startPoint = std::chrono::steady_clock::now();
    this->stopPoint = std::chrono::steady_clock::now();
}

void Timer::start() {

    this->startPoint = std::chrono::steady_clock::now();
}
void Timer::stop() {

    this->stopPoint = std::chrono::steady_clock::now();
}
void Timer::update() {

    this->startPoint = this->stopPoint;
    this->stop();
}

float Timer::getInterval() const {

    return (float)std::chrono::duration_cast<std::chrono::nanoseconds>(this->stopPoint - this->startPoint).count() / 1'000'000.0f;
}



MultiTimer::MultiTimer(int intervalCount) {

    this->currentIndex = 0;
    this->intervalCount = intervalCount;
    this->intervals = std::vector<float>(intervalCount);
}

void MultiTimer::update() {

    Timer::update();

    this->intervals[this->currentIndex] = Timer::getInterval();
    this->currentIndex = (this->currentIndex + 1) % this->intervalCount;
}

float MultiTimer::getAverageInterval() const {

    return std::accumulate(this->intervals.begin(), this->intervals.begin() + this->intervalCount, 0.0f) / this->intervalCount;
}



