#include "Includes.h"

#include "Window.h"
#include "Mouse.h"
#include "Keyboard.h"

#include "Font.h"
#include "Renderer2D.h"

#include "Timer.h"

#include "NeuralNetwork.h"



double func(double x) {

	return sin(x);
}



struct DataInfo {
	double minVal;
	double range;
};



void drawNetwork(NeuralNetwork& network, Font& font, NeuralNetworkDataset& dataset, int index, int trainingSteps) {

	network.predict(dataset.inputs[index]);

	Vector2f offset(100, 50);
	Vector2f size = Window::getInstance().size - offset;

	double dx = size.x / (network.profile.size() - 1);

	std::vector<std::vector<Vector2f>> positions(network.profile.size());
	for (int i = 0; i <= network.profile.size() - 1; i++) {

		double dy = size.y / (network.profile[i] + 1);

		for (int j = 0; j < network.profile[i]; j++) {
			positions[i].push_back(Vector2f(dx * i, dy * (j + 1)) + offset / 2);
		}
	}

	for (int i = 0; i < positions.size() - 1; i++) {
		for (int j = 0; j < positions[i].size(); j++) {
			for (int k = 0; k < positions[i + 1].size(); k++) {

				double weight = network.weights[i].data[j * positions[i + 1].size() + k];
				Vector4f color = weight > 0 ? Vector4f(1, 0, 0, 1) : Vector4f(0, 1, 0, 1);

				Renderer2D::getInstance().drawPointLine(positions[i][j], positions[i + 1][k], color, std::max(1.0, std::abs(weight)));
			}
		}
	}

	for (int i = 0; i < positions.size(); i++) {
		for (int j = 0; j < positions[i].size(); j++) {

			std::string text1 = std::to_string(network.layersInput[i][j]);
			std::string text2 = std::to_string(network.layersOutput[i][j]);
			text1 = text1.substr(0, text1.size() - 3);
			text2 = text2.substr(0, text2.size() - 3);

			double c = std::abs(tanh(network.layersOutput[i][j]));
			double f = c > 0.6 ? 0.5 : 1;

			Vector4f circleColor = Vector4f(c, c, c, 1);
			Vector4f text1Color = network.layersInput[i][j] > 0 ? Vector4f(0, f, 0, 1) : Vector4f(f, 0, 0, 1);
			Vector4f text2Color = network.layersOutput[i][j] > 0 ? Vector4f(0, f, 0, 1) : Vector4f(f, 0, 0, 1);

			Renderer2D::getInstance().drawCircle(positions[i][j], 30, Vector4f(0.4, 0.6, 0.8, 1), circleColor, -3);
			Renderer2D::getInstance().drawText(font, text1, positions[i][j], 16, text1Color, TEXT_ALIGN::MIDBOTTOM);
			Renderer2D::getInstance().drawText(font, text2, positions[i][j], 16, text2Color, TEXT_ALIGN::MIDTOP);

			if (i == positions.size() - 1) {

				std::string expected = std::to_string(dataset.outputs[index][j]);
				expected = expected.substr(0, expected.size() - 3);

				Renderer2D::getInstance().drawText(font, expected, positions[i][j] + Vector2f(0, 40), 20, Vector4f(0, 0, 0, 1), TEXT_ALIGN::CENTER);
			}
		}
	}

	double averageError = network.getAverageError(dataset);
	double error = network.getError(dataset.inputs[index], dataset.outputs[index]);

	std::string errorString = "Error : " + std::to_string(error);
	std::string averageErrorString = "Average Error : " + std::to_string(averageError);

	Renderer2D::getInstance().drawText(font, averageErrorString, Vector2f(5, 0), 24, Vector4f(0, 0, 0, 1), TEXT_ALIGN::TOPLEFT);
	Renderer2D::getInstance().drawText(font, errorString, Vector2f(5, 25), 24, Vector4f(0, 0, 0, 1), TEXT_ALIGN::TOPLEFT);
	Renderer2D::getInstance().drawText(font, std::to_string(trainingSteps), Vector2f(5, 45), 24, Vector4f(0, 0, 0, 1), TEXT_ALIGN::TOPLEFT);
}

void drawGraph(NeuralNetwork& network, Font& font, NeuralNetworkDataset& dataset, int index, DataInfo inputData, DataInfo outputData) {

	Vector2f offset(100, 50);
	Vector2f size = Window::getInstance().size - offset;

	Renderer2D::getInstance().drawRectangle(Window::getInstance().size / 2, size, Vector4f(0.3, 0.3, 0.3, 1), Vector4f(0.9, 0.9, 0.9, 1), -1);

	float dx = size.x / 10;
	float dy = size.y / 10;
	for (int i = 0; i < 9; i++) {
		Renderer2D::getInstance().drawPointLine(Vector2f(dx + dx * i, 10) + offset / 2, Vector2f(dx + dx * i, size.y - 10) + offset / 2, Vector4f(0.7, 0.7, 0.7, 1), 1);
		Renderer2D::getInstance().drawPointLine(Vector2f(10, dy + dy * i) + offset / 2, Vector2f(size.x - 10, dy + dy * i) + offset / 2, Vector4f(0.7, 0.7, 0.7, 1), 1);
	}

	std::vector<Vector2f> points;
	for (double i = 0; i <= 1; i += 0.001) {
		network.predict({ i });
		points.push_back(Vector2f(i, network.layersOutput.back().data[0]));
	}

	for (int i = 0; i < dataset.inputs.size(); i++) {

		double x = dataset.inputs[i].data[0];
		double y = 1 - dataset.outputs[i].data[0];

		Renderer2D::getInstance().drawCircle(Vector2f(x, y) * (size - 20) + offset / 2 + 10, 3, Vector4f(0, 0, 0, 1), Vector4f(0, 0, 0, 1), -1);
	}

	for (int i = 0; i < points.size(); i++) {

		float x = points[i].x;
		float y = 1 - points[i].y;

		Vector2f point = Vector2f(x, y) * (size - 20) + offset / 2 + 10;

		Renderer2D::getInstance().drawCircle(point, 1, Vector4f(1, 0, 0, 1), Vector4f(1, 0, 0, 1), -1);
	}
}

DataInfo normalizeData(std::vector<VectorDynamic<double>>& values) {

	double minVal = INT_MAX;
	double maxVal = INT_MIN;
	for (int i = 0; i < values.size(); i++) {
		for (int j = 0; j < values[i].data.size(); j++) {
			minVal = std::min(minVal, values[i].data[j]);
			maxVal = std::max(maxVal, values[i].data[j]);
		}
	}

	for (int i = 0; i < values.size(); i++) {
		for (int j = 0; j < values[i].data.size(); j++) {
			values[i].data[j] = (values[i].data[j] - minVal) / (maxVal - minVal);
		}
	}

	return { minVal, maxVal - minVal };
}



int main() {

	Window::getInstance().initialize(Vector2i(1600, 900), "Polynomial Regression");
	glOrtho(0, Window::getInstance().size.x, Window::getInstance().size.y, 0, -1, 1);
	glfwSwapInterval(0);

	Font font("consola");

	Mouse& mouse = Mouse::getInstance();
	Keyboard& keyboard = Keyboard::getInstance();



	NeuralNetwork network({ 1,64,64,1 }, 0.001, WeightInitialization::HE, ActivationFunction::SIGMOID);

	

	std::random_device device;
	std::mt19937 generator(device() & 0);
	std::uniform_real_distribution<> distribution(-0.1, 0.1);

	NeuralNetworkDataset dataset;
	for (double i = -3; i <= 3; i += 0.1) {
		for (int j = 0; j < 10; j++) {
			dataset.inputs.push_back({ i + distribution(generator) });
			dataset.outputs.push_back({ func(i + distribution(generator)) });
		}
	}

	DataInfo inputData = normalizeData(dataset.inputs);
	DataInfo outputData = normalizeData(dataset.outputs);

	int mode = 0;
	int inputIndex = 0;
	int trainingSteps = 0;

	for (int i = 0; i < 10000; i++) {
		network.train(dataset);
		trainingSteps += 1;

		if (i % 100 == 0) {
			printf("%f %%, %f MSE\n", i * 100.0 / 10000, network.getAverageError(dataset));
		}
	}

	int paramCount = 0;
	for (int i = 0; i < network.profile.size() - 1; i++) {
		paramCount += network.profile[i] * network.profile[i + 1];
	}

	printf("%d\n", paramCount);

	MultiTimer timer(100);
	while (!glfwWindowShouldClose(Window::getInstance().getWindow())) {

		glfwPollEvents(); 

		mouse.update();
		keyboard.update();
		timer.update();

		if (keyboard.pressed(GLFW_KEY_UP)) {
			inputIndex = (inputIndex + 1) % dataset.inputs.size();
		}
		if (keyboard.pressed(GLFW_KEY_DOWN)) {
			inputIndex = (inputIndex - 1 + dataset.inputs.size()) % dataset.inputs.size();
		}

		if (keyboard.pressed(GLFW_KEY_M)) {
			mode = (mode + 1) % 2;
		}

		if (keyboard.held(GLFW_KEY_SPACE)) {

			Timer t;
			while (true) {

				network.train(dataset);
				t.stop();
				trainingSteps += 1;

				if (t.getInterval() > 5) { break; }
			}
		}

		glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
		glClearColor(0.2, 0.4, 0.6, 1);

		if (mode == 0) {
			drawNetwork(network, font, dataset, inputIndex, trainingSteps);
		}
		else if (mode == 1 && network.profile.front() == 1 && network.profile.back() == 1) {
			drawGraph(network, font, dataset, inputIndex, inputData, outputData);
		}

		glfwSwapBuffers(Window::getInstance().getWindow());

		Window::getInstance().setTitle(std::to_string(timer.getAverageInterval()));
	}

	glfwTerminate();

	return 0;
}