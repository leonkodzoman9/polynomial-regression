#include "BufferDescriptor.h"

#include "Buffer.h"



BufferDescriptor::BufferDescriptor() {

    glGenVertexArrays(1, &this->ID);
}
BufferDescriptor::~BufferDescriptor() {

    glDeleteVertexArrays(1, &this->ID);
}

BufferDescriptor::BufferDescriptor(BufferDescriptor&& rhs) noexcept {

    if (this != &rhs) {

        this->ID = std::exchange(rhs.ID, 0);
    }
}
BufferDescriptor& BufferDescriptor::operator = (BufferDescriptor&& rhs) noexcept {

    if (this != &rhs) {

        glDeleteVertexArrays(1, &this->ID);

        this->ID = std::exchange(rhs.ID, 0);
    }

    return *this;
}
