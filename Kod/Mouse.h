#pragma once

#include "Includes.h"

#include "Vector.h"



void scrollCallback(GLFWwindow* window, double xOffset, double yOffset);



class Mouse {

private:

	double scrollAmount;

	std::array<bool, GLFW_MOUSE_BUTTON_LAST + 1> currentState;
	std::array<bool, GLFW_MOUSE_BUTTON_LAST + 1> previousState;

	std::array<std::pair<float, float>, GLFW_MOUSE_BUTTON_LAST + 1> pressTransitionCounter;
	std::array<std::pair<float, float>, GLFW_MOUSE_BUTTON_LAST + 1> releaseTransitionCounter;

	float currentTime;

	Vector2d previousPosition;
	Vector2d currentPosition;

	Mouse();
	Mouse(const Mouse& rhs) = delete;
	Mouse& operator=(const Mouse& rhs) = delete;
	Mouse(Mouse&& rhs) noexcept = delete;
	Mouse& operator=(Mouse&& rhs) noexcept = delete;

	void updatePressTransitionCounter(uint32_t button, float currentTime);
	void updateReleaseTransitionCounter(uint32_t button, float currentTime);

public:

	static Mouse& getInstance();

	void update();
	void reset();

	Vector2d position() const;
	Vector2d deltaPosition() const;

	double scroll() const;

	bool held(uint32_t button) const;
	bool pressed(uint32_t button) const;
	bool released(uint32_t button) const;

	float heldFor(uint32_t button) const;
	float releasedFor(uint32_t button) const;

	float pressDelta(uint32_t button) const;
	float releaseDelta(uint32_t button) const;
};


