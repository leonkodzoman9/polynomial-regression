#include "NeuralNetwork.h"



double sigmoid(double x) {
	return 1.0 / (1.0 + std::exp(-x));
}
double sigmoidDerivative(double x) {
	double temp = sigmoid(x);
	return temp * (1.0 - temp);
}

double tanhFunc(double x) {
	return std::tanh(x);
}
double tanhDerivative(double x) {
	double temp = tanhFunc(x);
	return 1 - temp * temp;
}

double leakyRelu(double x) {
	return x > 0 ? x : 0.01 * x;
}
double leakyReluDerivative(double x) {
	return x > 0 ? 1 : 0.01;
}



void NeuralNetwork::updateWeights() {

	for (int i = 0; i < this->weights.size(); i++) {
		this->weights[i] -= this->deltas[i] * this->learningRate;
		this->deltas[i] *= 0;
	}
}

NeuralNetwork::NeuralNetwork() {}
NeuralNetwork::NeuralNetwork(std::vector<int> profile, double learningRate, WeightInitialization initialization, ActivationFunction activation) {
	
	this->learningRate = learningRate;

	if (activation == ActivationFunction::LEAKY_RELU) {
		this->activationFunction = leakyRelu;
		this->activationFunctionDerivative = leakyReluDerivative;
	}
	else if (activation == ActivationFunction::SIGMOID) {
		this->activationFunction = sigmoid;
		this->activationFunctionDerivative = sigmoidDerivative;
	}
	else if (activation == ActivationFunction::TANH) {
		this->activationFunction = tanhFunc;
		this->activationFunctionDerivative = tanhDerivative;
	}

	this->profile = profile;
	for (int i = 0; i < profile.size(); i++) {
		this->layersInput.push_back(VectorDynamic<double>(profile[i]));
		this->layersOutput.push_back(VectorDynamic<double>(profile[i]));
	}

	std::random_device device;
	std::mt19937 generator(device());
	std::uniform_real_distribution<> uniform(0, 1);

	for (int i = 0; i < profile.size() - 1; i++) {

		std::normal_distribution<> normal(0, std::sqrt(2.0 / profile[i]));

		this->weights.push_back(MatrixDynamic<double>(profile[i + 1], profile[i]));
		this->weights.back().data = VectorDynamic<double>(this->weights.back().data.size);
		for (int j = 0; j < this->weights.back().data.size; j++) {

			if (initialization == WeightInitialization::NEGATIVE_ONE_TO_ONE) {
				this->weights.back().data[j] = uniform(generator) * 2 - 1;
			}
			else if (initialization == WeightInitialization::XAVIER) {
				double x = 1.0 / std::sqrt(profile[i]);
				this->weights.back().data[j] = uniform(generator) * 2 * x - x;
			}
			else if (initialization == WeightInitialization::HE) {
				this->weights.back().data[j] = normal(generator);
			}
		}

		this->biases.push_back(VectorDynamic<double>(profile[i + 1]));
		for (int j = 0; j < this->biases.back().size; j++) {
			this->biases.back().data[j] = uniform(generator) * 2 - 1;
		}

		this->deltas.push_back(MatrixDynamic<double>(profile[i + 1], profile[i]));
	}
}

void NeuralNetwork::predict(VectorDynamic<double> inputs) {

	this->layersOutput[0] = inputs;
	for (int i = 0; i < this->weights.size(); i++) {
		this->layersInput[i + 1] = this->weights[i] * this->layersOutput[i] + this->biases[i];
		this->layersOutput[i + 1] = VectorDynamicMath::Apply(this->layersInput[i + 1], this->activationFunction);
	}
}

void NeuralNetwork::train(VectorDynamic<double> inputs, VectorDynamic<double> outputs) {

	this->predict(inputs);

	VectorDynamic<double> part1 = -(outputs - this->layersOutput.back());
	VectorDynamic<double> part2 = VectorDynamicMath::Apply(this->layersInput.back(), this->activationFunctionDerivative);
	VectorDynamic<double>& part3 = this->layersOutput[this->profile.size() - 2];
	this->deltas.back() += MatrixDynamicMath::CartesianProduct(part1 * part2, part3);

	for (int layer = this->profile.size() - 2; layer > 0; layer--) {

		MatrixDynamic<double> transposed = MatrixDynamicMath::Transpose(this->weights[layer].multiply(this->deltas[layer]));
		VectorDynamic<double> part1 = transposed * VectorDynamic<double>(this->profile[layer + 1], 1);
		VectorDynamic<double> part2 = VectorDynamicMath::Apply(this->layersInput[layer], this->activationFunctionDerivative);
		VectorDynamic<double>& part3 = this->layersOutput[layer - 1];
		this->deltas[layer - 1] += MatrixDynamicMath::CartesianProduct(part1 * part2, part3);
	}

	this->updateWeights();
}

void NeuralNetwork::train(NeuralNetworkDataset dataset) {

	for (int i = 0; i < dataset.inputs.size(); i++) {
		this->train(dataset.inputs[i], dataset.outputs[i]);
	}
}

double NeuralNetwork::getError(VectorDynamic<double> inputs, VectorDynamic<double> outputs) {

	this->predict(inputs);

	VectorDynamic<double> error = -(outputs - this->layersOutput.back());

	return VectorDynamicMath::Dot(error, error);
}
double NeuralNetwork::getAverageError(NeuralNetworkDataset dataset) {

	double averageError = 0;
	for (int i = 0; i < dataset.inputs.size(); i++) {
		averageError += this->getError(dataset.inputs[i], dataset.outputs[i]);
	}

	return averageError / dataset.inputs.size();
}

