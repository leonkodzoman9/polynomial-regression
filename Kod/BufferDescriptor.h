#pragma once

#include "Includes.h"

#include "Buffer.h"



class BufferDescriptor {

public:

    uint32_t ID;

    BufferDescriptor();
    ~BufferDescriptor();

    BufferDescriptor(const BufferDescriptor& rhs) = delete;
    BufferDescriptor& operator=(const BufferDescriptor& rhs) = delete;
    BufferDescriptor(BufferDescriptor&& rhs) noexcept;
    BufferDescriptor& operator=(BufferDescriptor&& rhs) noexcept;
};
