#include "Window.h"

#include "Vector.h"



void windowResizeCallback(GLFWwindow* window, int width, int height) {

    glViewport(0, 0, width, height);
}



Window::Window() {

    this->title = "Window";
    this->window = nullptr;
}
Window::~Window() {

    glfwTerminate();
}

Window& Window::getInstance() {

    // A static instance becomes a singleton, first time initialized, other times just returned
    static Window instance;

    return instance;
}

void Window::initialize(Vector2i windowSize, const std::string& windowName) {
    
    this->size = windowSize;
    this->title = windowName;

    glfwInit();
    glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 4);
    glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 6);
    glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_COMPAT_PROFILE);

    this->window = glfwCreateWindow(windowSize.x, windowSize.y, windowName.data(), nullptr, nullptr);
    glfwMakeContextCurrent(this->window);

    glfwSetWindowSizeCallback(this->window, windowResizeCallback);

    gladLoadGLLoader((GLADloadproc)glfwGetProcAddress);

    glViewport(0, 0, windowSize.x, windowSize.y);
}

GLFWwindow* Window::getWindow() {

    return this->window;
}

std::string Window::getTitle() const {

    return this->title;
}
void Window::setTitle(const std::string& windowName) {

    this->title = windowName;

    glfwSetWindowTitle(this->window, windowName.data());
}

void Window::setMouseState(uint32_t state) {

    glfwSetInputMode(Window::getInstance().getWindow(), GLFW_CURSOR, state);
}