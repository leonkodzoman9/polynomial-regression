#pragma once

#include "Includes.h"



class Buffer {

public:

    uint32_t ID;
    uint32_t target;
    
    Buffer();
    Buffer(uint32_t target);
    ~Buffer();

    Buffer(const Buffer& rhs) = delete;
    Buffer& operator=(const Buffer& rhs) = delete;
    Buffer(Buffer&& rhs) noexcept;
    Buffer& operator=(Buffer&& rhs) noexcept;
};










