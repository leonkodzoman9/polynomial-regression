#pragma once

#include "Includes.h"

#include "Vector.h"



template <class DataType, int Rows, int Columns>
struct MatrixMembers {
    union {
        Vector<DataType, Rows * Columns> data;
        Vector<Vector<DataType, Columns>, Rows> rows;
    };
    MatrixMembers() {}
};



template <class DataType, int Rows, int Columns>
class Matrix : public MatrixMembers<DataType, Rows, Columns> {

public:

    // Members are generated at compile time

    // Constructors

    Matrix() {

        for (int i = 0; i < Rows * Columns; i++) {
            this->data[i] = (DataType)0;
        }
    }

    template <int Size = Rows> requires (Rows == Columns)
        explicit Matrix(DataType val) {

        for (int i = 0; i < Size; i++) {
            for (int j = 0; j < Size; j++) {
                this->rows.data[i].data[j] = (i == j) ? val : (DataType)0;
            }
        }
    }

    template <class... ValueType> requires (sizeof...(ValueType) == (Rows * Columns))
        explicit Matrix(ValueType... values) {

        int i = 0;
        ([&](auto& value) { this->data[i++] = (DataType)value; } (values), ...);
    }

    template <class... VectorDataType> requires (sizeof...(VectorDataType) == Rows)
        explicit Matrix(Vector<VectorDataType, Columns>... vectors) {
        
        int i = 0;
		([&](auto& vector) { this->rows.data[i++] = vector; } (vectors), ...);
    }

    // Convert from any size of same type

    template <int Rows2, int Columns2>
    explicit Matrix(const Matrix<DataType, Rows2, Columns2>& mat) {

        int i = 0;
        for (; i < std::min(Rows, Rows2); i++) {

            int j = 0;
            for (; j < std::min(Columns, Columns2); j++) {
                this->rows.data[i].data[j] = mat.rows.data[i].data[j];
            }
            for (; j < Columns; j++) {
                this->rows.data[i].data[j] = (DataType)0;
            }
        }

        for (; i < Rows; i++) {
            for (int j = 0; j < Columns; j++) {
                this->rows.data[i].data[j] = (DataType)0;
            }
        }
    }

    // Convert from any type of same size

    template <class DataType2>
    Matrix(const Matrix<DataType2, Rows, Columns>& mat) {

        for (int i = 0; i < Rows; i++) {
            for (int j = 0; j < Columns; j++) {
                this->rows.data[i].data[j] = (DataType)mat.rows.data[i];
            }
        }
    }

    // Access operator 

    Vector<DataType, Columns>& operator[](int index) {

        return this->rows.data[index];
    }

    // Addition

    Matrix operator+(const Matrix& mat) const {

        Matrix result;
        result.data = this->data + mat.data;

        return result;
    }
    Matrix operator+(DataType val) const {

        Matrix result;
        result.data = this->data + val;

        return result;
    }
    Matrix& operator+=(const Matrix& mat) {

        this->data += mat.data;

        return *this;
    }
    Matrix& operator+=(DataType val) {

        this->data += val;

        return *this;
    }

    // Subtraction

    Matrix operator-(const Matrix& mat) const {

        Matrix result;
        result.data = this->data - mat.data;

        return result;
    }
    Matrix operator-(DataType val) const {

        Matrix result;
        result.data = this->data - val;

        return result;
    }
    Matrix& operator-=(const Matrix& mat) {

        this->data -= mat.data;

        return *this;
    }
    Matrix& operator-=(DataType val) {

        this->data -= val;

        return *this;
    }

    // Multiplication (per-element)

    Matrix multiply(const Matrix& mat) const {

        Matrix result;
        result.data = this->data * mat.data;

        return result;
    }
    Matrix operator*(DataType val) const {

        Matrix result;
        result.data = this->data * val;

        return result;
    }
    Matrix& multiplied(const Matrix& mat) {

        this->data *= mat.data;

        return *this;
    }
    Matrix& operator*=(DataType val) {

        this->data *= val;

        return *this;
    }

    // Multiplication, generic case

    template <int Columns2> requires (!std::is_same<DataType, float>::value || Rows != 4 || Columns != 4 || Columns2 != 4)
        Matrix<DataType, Rows, Columns2> operator*(const Matrix<DataType, Columns, Columns2>& mat) const {

        Matrix<DataType, Rows, Columns2> result;
        for (int i = 0; i < Rows; i++) {
            for (int j = 0; j < Columns2; j++) {
                for (int k = 0; k < Columns; k++) {
                    result.rows.data[i].data[j] += this->rows.data[i].data[k] * mat.rows.data[k].data[j];
                }
            }
        }

        return result;
    }
    template <int Size = Rows> requires (!std::is_same<DataType, float>::value && Rows == Columns)
        Matrix<DataType, Size, Size>& operator*=(const Matrix<DataType, Size, Size>& mat) {

        *this = *this * mat;

        return *this;
    }

    // Multiplication, optimized for 4x4 float matrix

    template <int Size = Rows> requires (std::is_same<DataType, float>::value&& Rows == 4 && Columns == 4)
        Matrix<DataType, Size, Size> operator*(const Matrix<DataType, Size, Size>& mat) const {

		Matrix<DataType, Size, Size> result;

		__m128 _matRows[4] = {
			_mm_loadu_ps((DataType*)&mat.rows.data[0]),
			_mm_loadu_ps((DataType*)&mat.rows.data[1]),
			_mm_loadu_ps((DataType*)&mat.rows.data[2]),
			_mm_loadu_ps((DataType*)&mat.rows.data[3])
		};

		for (int i = 0; i < 4; i++) {
			__m128 _r = _mm_loadu_ps((DataType*)&this->rows.data[i]);
			__m128 _ps0 = _mm_mul_ps(_mm_shuffle_ps(_r, _r, 0b00000000), _matRows[0]);
			__m128 _ps1 = _mm_mul_ps(_mm_shuffle_ps(_r, _r, 0b01010101), _matRows[1]);
			__m128 _ps2 = _mm_mul_ps(_mm_shuffle_ps(_r, _r, 0b10101010), _matRows[2]);
			__m128 _ps3 = _mm_mul_ps(_mm_shuffle_ps(_r, _r, 0b11111111), _matRows[3]);
			_mm_storeu_ps((DataType*)&result.rows.data[i], _mm_add_ps(_mm_add_ps(_ps0, _ps1), _mm_add_ps(_ps2, _ps3)));
		}

        return result;
    }
    template <int Size = Rows> requires (std::is_same<DataType, float>::value && Rows == 4 && Columns == 4)
        Matrix<DataType, Size, Size>& operator*=(const Matrix<DataType, Size, Size>& mat) {

        *this = *this * mat;

        return *this;
    }

    // Division

    Matrix operator/(const Matrix& mat) const {

        Matrix result;
        result.data = this->data / mat.data;

        return result;
    }
    Matrix operator/(DataType val) const {

        Matrix result;
        result.data = this->data / val;

        return result;
    }
    Matrix& operator/=(const Matrix& mat) {

        this->data /= mat.data;

        return *this;
    }
    Matrix& operator/=(DataType val) {

        this->data /= val;

        return *this;
    }

    // Assignment, from any type of same size

    template <class DataType2>
    Matrix<DataType, Rows, Columns>& operator=(const Matrix<DataType2, Rows, Columns>& mat) {

        this->data = mat.data;

        return *this;
    }

    // Negation

    Matrix operator-() const {

        return *this * -1;
    } 

    // Matrix-Vector multiplication

    Vector<DataType, Rows> operator*(const Vector<DataType, Columns>& vec) const {

        Vector<DataType, Rows> result;
        for (int i = 0; i < Rows; i++) {
            result.data[i] = VectorMath::Dot(this->rows.data[i], vec);
        }

        return result;
    }

    // Determinant, only square matrices

    template <int Size = Rows> requires (Rows == Columns)
    DataType determinant() {

        Matrix<DataType, Size, Size> temp = *this;

        DataType determinant = 1;

        for (int i = 0; i < Size - 1; i++) {
            if (temp.rows.data[i].data[i] == 0) {
                for (int j = i + 1; j < Size; j++) {
                    if (temp.rows.data[j].data[i] != 0) {
                        std::swap(temp.rows.data[j], temp.rows.data[i]);
                        determinant *= -1;
                        break;
                    }
                }
            }
        }

        for (int i = 0; i < Size - 1; i++) {
            for (int j = i + 1; j < Size; j++) {
                DataType factor = -temp.rows.data[j].data[i] / temp.rows.data[i].data[i];
                temp.rows.data[j] += temp.rows.data[i] * factor;
            }
        }

        for (int i = 0; i < Size; i++) {
            determinant *= temp.rows.data[i].data[i];
        }

        return determinant;
    }
};



typedef class Matrix<int, 2, 2> Matrix2i;
typedef class Matrix<int, 3, 3> Matrix3i;
typedef class Matrix<int, 4, 4> Matrix4i;

typedef class Matrix<int64_t, 2, 2> Matrix2l;
typedef class Matrix<int64_t, 3, 3> Matrix3l;
typedef class Matrix<int64_t, 4, 4> Matrix4l;

typedef class Matrix<float, 2, 2> Matrix2f;
typedef class Matrix<float, 3, 3> Matrix3f;
typedef class Matrix<float, 4, 4> Matrix4f;

typedef class Matrix<double, 2, 2> Matrix2d;
typedef class Matrix<double, 3, 3> Matrix3d;
typedef class Matrix<double, 4, 4> Matrix4d;



namespace MatrixMath {

    template <class DataType, int Rows, int Columns>
    static Matrix<DataType, Columns, Rows> Transpose(const Matrix<DataType, Rows, Columns>& mat) {

        Matrix<DataType, Columns, Rows> result;

        for (int i = 0; i < Rows; i++) {
            for (int j = 0; j < Columns; j++) {
                result.rows.data[j].data[i] = mat.rows.data[i].data[j];
            }
        }

        return result;
    }
    
    template <class DataType, int Size>
    static Matrix<DataType, Size, Size> Invert(const Matrix<DataType, Size, Size>& mat) {

        Matrix<DataType, Size, Size> original = mat;
        Matrix<DataType, Size, Size> result(1);

        for (int i = 0; i < 3; i++) {
            if (original.rows.data[i].data[i] == 0) {
                for (int j = i + 1; j < 4; j++) {
                    if (original.rows.data[j].data[i] != 0) {
                        std::swap(original.rows.data[i], original.rows.data[j]);
                        std::swap(result.rows.data[i], result.rows.data[j]);
                        break;
                    }
                }
            }
        }

        for (int i = 0; i < 4; i++) {
            for (int j = 0; j < 4; j++) {
                if (i != j) {

                    DataType factor = -original.rows.data[j].data[i] / original.rows.data[i].data[i];

                    original.rows.data[j] += original.rows.data[i] * factor;
                    result.rows.data[j] += result.rows.data[i] * factor;
                }
            }
        }

        for (int i = 0; i < 4; i++) {
            result.rows.data[i] /= original.rows.data[i].data[i];
        }

        return result;
    }

    template <class DataType, int Count1, int Count2>
    static Matrix<DataType, Count1, Count2> CartesianProduct(const Vector<DataType, Count1>& vec1, const Vector<DataType, Count2>& vec2) {

        Matrix<DataType, Count1, Count2> result;
        for (int i = 0; i < Count1; i++) {
            for (int j = 0; j < Count2; j++) {
                result.rows[i].data[j] = vec1.data[i] * vec2.data[j];
            }
        }

        return result;
    }

    template <class DataType>
    Matrix<DataType, 4, 4> Scaling(Vector<DataType, 3> scale) {

        return Matrix<DataType, 4, 4>(scale.x, 0, 0, 0, 0, scale.y, 0, 0, 0, 0, scale.z, 0, 0, 0, 0, 1);
    }
    template <class DataType>
    Matrix<DataType, 4, 4> Scaling(Vector<DataType, 4> scale) {

        return Matrix<DataType, 4, 4>(scale.x, 0, 0, 0, 0, scale.y, 0, 0, 0, 0, scale.z, 0, 0, 0, 0, scale.w);
    }

    template <class DataType>
    Matrix<DataType, 4, 4> Translation(Vector<DataType, 3> translation) {

        return Matrix<DataType, 4, 4>(1, 0, 0, translation.x, 0, 1, 0, translation.y, 0, 0, 1, translation.z, 0, 0, 0, 1);
    }

    template <class DataType>
    Matrix<DataType, 4, 4> Rotation(DataType angle, const Vector<DataType, 3>& axisOriginal) {

        Matrix<DataType, 4, 4> result;

        Vector<DataType, 3> axis = VectorMath::Normalize(axisOriginal);

        DataType angleSine = (DataType)std::sin(angle);
        DataType angleCosine = (DataType)std::cos(angle);
        DataType oneMinCosine = (DataType)1 - angleCosine;

        result.rows.data[0].data[0] = angleCosine + axis.x * axis.x * oneMinCosine;
        result.rows.data[0].data[1] = axis.x * axis.y * oneMinCosine - axis.z * angleSine;
        result.rows.data[0].data[2] = axis.x * axis.z * oneMinCosine + axis.y * angleSine;

        result.rows.data[1].data[0] = axis.y * axis.x * oneMinCosine + axis.z * angleSine;
        result.rows.data[1].data[1] = angleCosine + axis.y * axis.y * oneMinCosine;
        result.rows.data[1].data[2] = axis.y * axis.z * oneMinCosine - axis.x * angleSine;

        result.rows.data[2].data[0] = axis.z * axis.x * oneMinCosine - axis.y * angleSine;
        result.rows.data[2].data[1] = axis.z * axis.y * oneMinCosine + axis.x * angleSine;
        result.rows.data[2].data[2] = angleCosine + axis.z * axis.z * oneMinCosine;

        result.rows.data[3].data[3] = (DataType)1;

        return result;
    }
    template <class DataType>
    Matrix<DataType, 4, 4> Rotation(DataType pitch, DataType yaw, DataType roll) {

        DataType siny = (DataType)std::sin(yaw);
        DataType cosy = (DataType)std::cos(yaw);
        DataType sinp = (DataType)std::sin(pitch);
        DataType cosp = (DataType)std::cos(pitch);
        DataType sinr = (DataType)std::sin(roll);
        DataType cosr = (DataType)std::cos(roll);

        Matrix<DataType, 4, 4> yawMatrix(cosy, -siny, 0, 0, siny, cosy, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1);
        Matrix<DataType, 4, 4> pitchMatrix(cosp, 0, sinp, 0, 0, 1, 0, 0, -sinp, 0, cosp, 0, 0, 0, 0, 1);
        Matrix<DataType, 4, 4> rollMatrix(1, 0, 0, 0, 0, cosr, -sinr, 0, 0, sinr, cosr, 0, 0, 0, 0, 1);

        return yawMatrix * pitchMatrix * rollMatrix;
    }
    template <class DataType>
    Matrix<DataType, 4, 4> Rotation(const Vector<DataType, 3>& angles) {

        DataType siny = (DataType)std::sin(angles.x);
        DataType cosy = (DataType)std::cos(angles.x);
        DataType sinp = (DataType)std::sin(angles.y);
        DataType cosp = (DataType)std::cos(angles.y);
        DataType sinr = (DataType)std::sin(angles.z);
        DataType cosr = (DataType)std::cos(angles.z);

        Matrix<DataType, 4, 4> yawMatrix(cosy, -siny, 0, 0, siny, cosy, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1);
        Matrix<DataType, 4, 4> pitchMatrix(cosp, 0, sinp, 0, 0, 1, 0, 0, -sinp, 0, cosp, 0, 0, 0, 0, 1);
        Matrix<DataType, 4, 4> rollMatrix(1, 0, 0, 0, 0, cosr, -sinr, 0, 0, sinr, cosr, 0, 0, 0, 0, 1);

        return yawMatrix * pitchMatrix * rollMatrix;
    }

    template <class DataType>
    Matrix<DataType, 4, 4> Orthographic(DataType left, DataType right, DataType top, DataType bottom, DataType near, DataType far) {

        return Matrix<DataType, 4, 4>(
            (DataType)2.0 / (right - left), (DataType)0, (DataType)0, -(right + left) / (right - left),
            (DataType)0, (DataType)2.0 / (top - bottom), 0, -(top + bottom) / (top - bottom),
            (DataType)0, (DataType)0, (DataType)-2.0 / (far - near), -(far + near) / (far - near),
            (DataType)0, (DataType)0, (DataType)0, (DataType)1
            );
    }
    template <class DataType>
    Matrix<DataType, 4, 4> View(Vector<DataType, 3> position, Vector<DataType, 3> front, Vector<DataType, 3> right, Vector<DataType, 3> up) {

        return Matrix<DataType, 4, 4>(
            Vector<DataType, 4>(right, -VectorMath::Dot(right, position)),
            Vector<DataType, 4>(up, -VectorMath::Dot(up, position)),
            Vector<DataType, 4>(-front, VectorMath::Dot(front, position)),
            Vector<DataType, 4>(0, 0, 0, 1)
            );
    }

    template <class DataType>
    Matrix<DataType, 4, 4> Projection(DataType aspect, DataType fov, DataType near, DataType far) {

        DataType halfFovTan = (DataType)std::tan(fov / (DataType)(180 / std::numbers::pi) / (DataType)2.0);

        Matrix<DataType, 4, 4> result;
        result.rows.data[0].data[0] = (DataType)1 / (aspect * halfFovTan);
        result.rows.data[1].data[1] = (DataType)1 / halfFovTan;
        result.rows.data[2].data[2] = -(far + near) / (far - near);
        result.rows.data[2].data[3] = (DataType)-2 * (far * near) / (far - near);
        result.rows.data[3].data[2] = (DataType)-1;

        return result;
    }
}











