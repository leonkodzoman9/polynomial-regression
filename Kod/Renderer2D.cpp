#include "Renderer2D.h"

#include "ShaderProgram.h"
#include "Buffer.h"
#include "BufferDescriptor.h"
#include "Vector.h"
#include "Font.h"
#include "Window.h"



Renderer2D::Renderer2D() {

    this->shaderLine.addShader("shaderPointLine.vert", GL_VERTEX_SHADER);
    this->shaderLine.addShader("shaderSolidColor.frag", GL_FRAGMENT_SHADER);
    this->shaderLine.createProgram();

    this->shaderEllipse.addShader("shaderEllipse.vert", GL_VERTEX_SHADER);
    this->shaderEllipse.addShader("shaderSolidColor.frag", GL_FRAGMENT_SHADER);
    this->shaderEllipse.createProgram();

    this->shaderTriangle.addShader("shaderTriangle.vert", GL_VERTEX_SHADER);
    this->shaderTriangle.addShader("shaderSolidColor.frag", GL_FRAGMENT_SHADER);
    this->shaderTriangle.createProgram();

    this->shaderRectangle.addShader("shaderRectangle.vert", GL_VERTEX_SHADER);
    this->shaderRectangle.addShader("shaderSolidColor.frag", GL_FRAGMENT_SHADER);
    this->shaderRectangle.createProgram();

    this->shaderFont.addShader("shaderFont.vert", GL_VERTEX_SHADER);
    this->shaderFont.addShader("shaderFont.frag", GL_FRAGMENT_SHADER);
    this->shaderFont.createProgram();

    this->viewBoundary = Vector4f(0, Window::getInstance().size.x, 0, Window::getInstance().size.y);
    this->updateOrthographicMatrix();
}
Renderer2D& Renderer2D::getInstance() {

    static Renderer2D instance;

    return instance;
}

void Renderer2D::updateOrthographicMatrix() {

    this->orthographicMatrix = MatrixMath::Orthographic(
        this->viewBoundary.x, this->viewBoundary.y,
        this->viewBoundary.z, this->viewBoundary.w,
        -1.0f, 1.0f
    );
}

Vector2f Renderer2D::getPositionInWorldSpace(Vector2f screenSpace) {

    float scale = (this->viewBoundary.y - this->viewBoundary.x) / Window::getInstance().size.x;

    return screenSpace * scale + Vector2f(this->viewBoundary.x, this->viewBoundary.z);
}
Vector2f Renderer2D::getPositionInScreenSpace(Vector2f worldSpace) {

    float scale = (this->viewBoundary.y - this->viewBoundary.x) / Window::getInstance().size.x;

    return (worldSpace - Vector2f(this->viewBoundary.x, this->viewBoundary.z)) / scale;
}

void Renderer2D::moveViewport(Vector2f direction) {

    float scale = (this->viewBoundary.y - this->viewBoundary.x) / Window::getInstance().size.x;

    direction *= scale;
    this->viewBoundary.x -= direction.x;
    this->viewBoundary.y -= direction.x;
    this->viewBoundary.z -= direction.y;
    this->viewBoundary.w -= direction.y;

    this->updateOrthographicMatrix();
}
void Renderer2D::zoomViewport(Vector2f zoomPoint, float zoomMultiplier) {

    zoomPoint = this->getPositionInWorldSpace(zoomPoint);

    float deltax1 = this->viewBoundary.x - zoomPoint.x;
    deltax1 *= 1 - zoomMultiplier;

    float deltax2 = this->viewBoundary.y - zoomPoint.x;
    deltax2 *= 1 - zoomMultiplier;

    float deltay1 = this->viewBoundary.z - zoomPoint.y;
    deltay1 *= 1 - zoomMultiplier;

    float deltay2 = this->viewBoundary.w - zoomPoint.y;
    deltay2 *= 1 - zoomMultiplier;

    this->viewBoundary = Vector4f(deltax1 + zoomPoint.x, deltax2 + zoomPoint.x, deltay1 + zoomPoint.y, deltay2 + zoomPoint.y);
    this->updateOrthographicMatrix();
}

void Renderer2D::drawPointLine(Vector2f point1, Vector2f point2, Vector4f color, float width) {

    this->shaderLine.use();
    glBindVertexArray(this->descriptorEmpty.ID);

    this->shaderLine.setMatrix4fArray("orthographicMatrix", &this->orthographicMatrix, 1, true);
    this->shaderLine.setVector2f("point1", point1);
    this->shaderLine.setVector2f("point2", point2);		  
    this->shaderLine.setVector4f("color", color);			  

    glLineWidth(width);		
    glDrawArrays(GL_LINES, 0, 2);							  
}
void Renderer2D::drawAngleLine(Vector2f point, float angle, float length, Vector4f color, float width) {

    this->drawPointLine(point, point + VectorMath::FromPolar(length, angle), color, width);
}

void Renderer2D::drawEllipse(Vector2f point, Vector2f radii, Vector4f outlineColor, Vector4f fillColor, float width, float angle, int vertexCount) {

    this->shaderEllipse.use();
    glBindVertexArray(this->descriptorEmpty.ID);

    this->shaderEllipse.setMatrix4fArray("orthographicMatrix", &this->orthographicMatrix, 1, true);   
    this->shaderEllipse.setVector2f("point", point);											   
    this->shaderEllipse.setVector2f("radii", radii);											   
    this->shaderEllipse.setFloat("angle", angle);												   
    this->shaderEllipse.setInt("divisions", vertexCount);										   

    if (width <= 0) {

        width = -width;
        this->shaderEllipse.setVector4f("color", fillColor);
        glDrawArrays(GL_TRIANGLE_FAN, 0, vertexCount * 3);
    }

    if (width > 0) {

        glLineWidth(width);
        this->shaderEllipse.setVector4f("color", outlineColor);
        glDrawArrays(GL_LINE_LOOP, 0, vertexCount);
    }
}
void Renderer2D::drawCircle(Vector2f point, float radius, Vector4f outlineColor, Vector4f fillColor, float width, int vertexCount) {

    this->drawEllipse(point, Vector2f(radius), outlineColor, fillColor, width, 0, vertexCount);
}

void Renderer2D::drawTriangle(Vector2f point1, Vector2f point2, Vector2f point3, Vector4f outlineColor, Vector4f fillColor, int width) {

    this->shaderTriangle.use();
    glBindVertexArray(this->descriptorEmpty.ID);

    this->shaderTriangle.setMatrix4fArray("orthographicMatrix", &this->orthographicMatrix, 1, true);
    this->shaderTriangle.setVector2f("point1", point1);
    this->shaderTriangle.setVector2f("point2", point2);
    this->shaderTriangle.setVector2f("point3", point3);

    if (width <= 0) {

        width = -width;
        this->shaderTriangle.setVector4f("color", fillColor);
        glDrawArrays(GL_TRIANGLES, 0, 3);
    }

    if (width > 0) {

        glLineWidth((float)width);
        this->shaderTriangle.setVector4f("color", outlineColor);
        glDrawArrays(GL_LINE_LOOP, 0, 3);
    }
}
void Renderer2D::drawRectangle(Vector2f center, Vector2f size, Vector4f outlineColor, Vector4f fillColor, int width, float angle) {

    this->shaderRectangle.use();
    glBindVertexArray(this->descriptorEmpty.ID);

    this->shaderRectangle.setMatrix4fArray("orthographicMatrix", &this->orthographicMatrix, 1, true);
    this->shaderRectangle.setVector2f("center", center);
    this->shaderRectangle.setVector2f("size", size);
    this->shaderRectangle.setFloat("angle", angle);

    if (width <= 0) {
    
        width = -width;
        this->shaderRectangle.setVector4f("color", fillColor);
        glDrawArrays(GL_TRIANGLE_FAN, 0, 6);
    }

    if (width > 0) {
    
        glLineWidth((float)width);
        this->shaderRectangle.setVector4f("color", outlineColor);
        glDrawArrays(GL_LINE_LOOP, 0, 4);
    }
}

void Renderer2D::drawText(Font& font, std::string text, Vector2f anchorPoint, float height, Vector4f color, TEXT_ALIGN textAlign) {

    int align = (int)textAlign % 9;

    glEnable(GL_BLEND);
    glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);

    this->shaderFont.use();
    this->shaderFont.setMatrix4fArray("orthographicMatrix", &this->orthographicMatrix, 1, true);
    this->shaderFont.setVector4f("textColor", color);

    glBindVertexArray(this->descriptorEmpty.ID);

    float scale = (float)height / font.getHeight();

    float textWidth = 0;
    for (int i = 0; i < text.size(); i++) {
        textWidth += font.getSymbol(text[i]).characterWidth;
    }
    textWidth *= scale;

    glActiveTexture(GL_TEXTURE0);

    float xOffset = 0;
    for (int i = 0; i < text.size(); i++) {

        Symbol& symbol = font.getSymbol(text[i]);
        symbol.texture.bind();

        float posX = anchorPoint.x + xOffset * scale;
        float posY = anchorPoint.y + (symbol.size.y - symbol.offset.y) * scale;

        Vector2f size = symbol.size * scale;

        Vector2f offset = Vector2f(TextOffsetX[align], TextOffsetY[align]) * Vector2f(textWidth, font.getHeight() * scale);

        Vector2f corners[4] = { 
            Vector2f(posX			, posY - size.y	) + offset,
            Vector2f(posX + size.x	, posY - size.y	) + offset,
            Vector2f(posX + size.x	, posY			) + offset,
            Vector2f(posX			, posY			) + offset 
        };
        xOffset += symbol.characterWidth;

        this->shaderFont.setVector2fArray("vertexPositions", corners, 4);

        if (text[i] == ' ') {
            continue;
        }

        symbol.texture.bind();
        glDrawArrays(GL_TRIANGLE_FAN, 0, 4);
    }

    glDisable(GL_BLEND);
}



