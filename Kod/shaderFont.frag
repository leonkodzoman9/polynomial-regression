#version 460 core

layout (location = 0) out vec4 finalColor;

in vec2 fragmentTextureCoordinate;

layout (binding = 0) uniform sampler2D textureSampler;

uniform vec4 textColor;

void main() {   

    float alpha = texture(textureSampler, fragmentTextureCoordinate).r;

    if (alpha < 0.5) {
        discard;
    }

    vec4 texturePixel = vec4(1.0, 1.0, 1.0, alpha);

    finalColor = textColor * texturePixel;
}  