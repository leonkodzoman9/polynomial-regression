#pragma once

#include "Includes.h"

#include "VectorDynamic.h"
#include "MatrixDynamic.h"



struct NeuralNetworkDataset {

	std::vector<VectorDynamic<double>> inputs;
	std::vector<VectorDynamic<double>> outputs;
};


enum class WeightInitialization {
	NEGATIVE_ONE_TO_ONE,
	XAVIER,
	HE
};

enum class ActivationFunction {
	SIGMOID,
	TANH,
	LEAKY_RELU
};

class NeuralNetwork {

private:

	double (*activationFunction)(double);
	double (*activationFunctionDerivative)(double);

	void updateWeights();

public:

	double learningRate = 1;
	std::vector<int> profile;

	std::vector<MatrixDynamic<double>> weights;
	std::vector<VectorDynamic<double>> biases;
	std::vector<MatrixDynamic<double>> deltas;
	std::vector<VectorDynamic<double>> layersInput;
	std::vector<VectorDynamic<double>> layersOutput;

	NeuralNetwork();
	NeuralNetwork(
		std::vector<int> profile, 
		double learningRate = 0.5, 
		WeightInitialization initialization = WeightInitialization::NEGATIVE_ONE_TO_ONE, 
		ActivationFunction activation = ActivationFunction::SIGMOID);

	void predict(VectorDynamic<double> inputs);

	void train(VectorDynamic<double> inputs, VectorDynamic<double> outputs);
	void train(NeuralNetworkDataset dataset);

	double getError(VectorDynamic<double> inputs, VectorDynamic<double> outputs);
	double getAverageError(NeuralNetworkDataset dataset);
};
