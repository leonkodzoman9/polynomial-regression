#pragma once

#include "Includes.h"



class Texture {

public:

    uint32_t ID;
    uint32_t target;

    Texture();
    Texture(uint32_t target);

    Texture(const Texture& rhs) = delete;
    Texture& operator=(const Texture& rhs) = delete;
    Texture(Texture&& rhs) noexcept;
    Texture& operator=(Texture&& rhs) noexcept;

    ~Texture();

    // Set data to the texture
    void setData(uint32_t targetType, uint32_t bufferFormat, int sourceWidth, int sourceHeight,
        uint32_t dataFormat, uint32_t dataColorFormat, void* data) const;

    // Set parameter
    void setParameter(uint32_t name, uint32_t value) const;

    // Generate mipmaps for the current texture
    void generateMipmaps() const;

    void bind() const;
    void unbind() const;

    uint32_t getID() const;
};


