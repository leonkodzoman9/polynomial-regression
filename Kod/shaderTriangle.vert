#version 460 core

uniform vec2 point1;
uniform vec2 point2;
uniform vec2 point3;
  
uniform mat4 orthographicMatrix;

void main() {

    vec2 pointArray[3] = {point1, point2, point3};

    gl_Position = orthographicMatrix * vec4(pointArray[gl_VertexID], 0.0, 1.0);
} 






