#pragma once

#include "Includes.h"

#include "VectorDynamic.h"



template <class DataType>
class MatrixDynamic {

public:

    int rows = 0;
    int columns = 0;
    VectorDynamic<DataType> data;

    // Constructors

    MatrixDynamic() {

    }
    explicit MatrixDynamic(int rows, int columns) {

        this->rows = rows;
        this->columns = columns;
        this->data = VectorDynamic<DataType>(rows * columns, 0);
    }

    // Convert from any type

    template <class DataType2>
    MatrixDynamic(const MatrixDynamic<DataType2>& mat) {

        this->rows = mat.rows;
        this->columns = mat.columns;
        this->data = VectorDynamic<DataType>(mat.rows * mat.columns, 0);
        for (int i = 0; i < this->data.data.size(); i++) {
            this->data.data[i] = (DataType)mat.data.data[i];
        }
    }

    // Addition

    MatrixDynamic operator+(const MatrixDynamic& mat) const {

        if (this->rows != mat.rows || this->columns != mat.columns) {
            throw;
        }

        MatrixDynamic result(this->rows, this->columns);
        result.data = this->data + mat.data;

        return result;
    }
    MatrixDynamic operator+(DataType val) const {

        MatrixDynamic result(this->rows, this->columns);
        result.data = this->data + val;

        return result;
    }
    MatrixDynamic& operator+=(const MatrixDynamic& mat) {

        if (this->rows != mat.rows || this->columns != mat.columns) {
            throw;
        }

        this->data += mat.data;

        return *this;
    }
    MatrixDynamic& operator+=(DataType val) {

        this->data += val;

        return *this;
    }

    // Subtraction

    MatrixDynamic operator-(const MatrixDynamic& mat) const {

        if (this->rows != mat.rows || this->columns != mat.columns) {
            throw;
        }

        MatrixDynamic result(this->rows, this->columns);
        result.data = this->data - mat.data;

        return result;
    }
    MatrixDynamic operator-(DataType val) const {

        MatrixDynamic result(this->rows, this->columns);
        result.data = this->data - val;

        return result;
    }
    MatrixDynamic& operator-=(const MatrixDynamic& mat) {

        if (this->rows != mat.rows || this->columns != mat.columns) {
            throw;
        }

        this->data -= mat.data;

        return *this;
    }
    MatrixDynamic& operator-=(DataType val) {

        this->data -= val;

        return *this;
    }

    // Multiplication (per-element)

    MatrixDynamic multiply(const MatrixDynamic& mat) const {

        if (this->rows != mat.rows || this->columns != mat.columns) {
            throw;
        }

        MatrixDynamic result(this->rows, this->columns);
        result.data = this->data * mat.data;

        return result;
    }
    MatrixDynamic operator*(DataType val) const {

        MatrixDynamic result(this->rows, this->columns);
        result.data = this->data * val;

        return result;
    }
    MatrixDynamic& multiplied(const MatrixDynamic& mat) {

        if (this->rows != mat.rows || this->columns != mat.columns) {
            throw;
        }

        this->data *= mat.data;

        return *this;
    }
    MatrixDynamic& operator*=(DataType val) {

        this->data *= val;

        return *this;
    }

    // Multiplication, generic case

    MatrixDynamic operator*(const MatrixDynamic& mat) const {

        if (this->columns != mat.rows) {
            throw;
        }

        MatrixDynamic result(this->rows, mat.columns);
        for (int i = 0; i < this->rows; i++) {
            for (int j = 0; j < mat.columns; j++) {
                for (int k = 0; k < this->columns; k++) {
                    result.data.data[i * mat.columns + j] += this->data.data[i * this->columns + k] * mat.data.data[k * mat.columns + j];
                }
            }
        }

        return result;
    }
    MatrixDynamic& operator*=(const MatrixDynamic& mat) {

        *this = *this * mat;

        return *this;
    }

    // Division

    MatrixDynamic operator/(const MatrixDynamic& mat) const {

        if (this->rows != mat.rows || this->columns != mat.columns) {
            throw;
        }

        MatrixDynamic result(this->rows, this->columns);
        result.data = this->data / mat.data;

        return result;
    }
    MatrixDynamic operator/(DataType val) const {

        MatrixDynamic result(this->rows, this->columns);
        result.data = this->data / val;

        return result;
    }
    MatrixDynamic& operator/=(const MatrixDynamic& mat) {

        if (this->rows != mat.rows || this->columns != mat.columns) {
            throw;
        }

        this->data /= mat.data;

        return *this;
    }
    MatrixDynamic& operator/=(DataType val) {

        this->data /= val;

        return *this;
    }

    // Assignment, from any type of same size

    template <class DataType2>
    MatrixDynamic<DataType>& operator=(const MatrixDynamic<DataType2>& mat) {

        this->rows = mat.rows;
        this->columns = mat.columns;
        this->data = VectorDynamic<DataType>(mat.rows * mat.columns, 0);
        for (int i = 0; i < this->data.data.size(); i++) {
            this->data.data[i] = (DataType)mat.data.data[i];
        }

        return *this;
    }

    // Negation

    MatrixDynamic operator-() const {

        return *this * -1;
    }

    // Matrix-Vector multiplication

    VectorDynamic<DataType> operator*(const VectorDynamic<DataType>& vec) const {

        if (this->columns != vec.data.size()) {
            throw;
        }

        VectorDynamic<DataType> result(this->rows);
        for (int i = 0; i < this->rows; i++) {
            for (int j = 0; j < this->columns; j++) {
                result.data[i] += this->data.data[i * this->columns + j] * vec.data[j];
            }
        }

        return result;
    }
};



namespace MatrixDynamicMath {

    template <class DataType>
    static MatrixDynamic<DataType> Transpose(const MatrixDynamic<DataType>& mat) {

        MatrixDynamic<DataType> result(mat.columns, mat.rows);

        for (int i = 0; i < result.rows; i++) {
            for (int j = 0; j < result.columns; j++) {
                result.data.data[j * result.rows + i] = mat.data.data[i * result.columns + j];
            }
        }

        return result;
    }

    template <class DataType>
    static MatrixDynamic<DataType> CartesianProduct(const VectorDynamic<DataType>& vec1, const VectorDynamic<DataType>& vec2) {

        MatrixDynamic<DataType> result(vec1.size, vec2.size);
        for (int i = 0; i < vec1.size; i++) {
            for (int j = 0; j < vec2.size; j++) {
                result.data.data[i * vec2.size + j] = vec1.data[i] * vec2.data[j];
            }
        }

        return result;
    }
}


