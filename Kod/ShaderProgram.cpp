#include "ShaderProgram.h"

#include "Vector.h"
#include "Matrix.h"



ShaderProgram::ShaderProgram() {

    this->shaderProgramID = 0;
}
ShaderProgram::~ShaderProgram() {

    glDeleteProgram(this->shaderProgramID);
}

void ShaderProgram::checkShaderForErrors(uint32_t shaderID) {

    int success;
    char infoLog[1024];
    glGetShaderiv(shaderID, GL_COMPILE_STATUS, &success);
    if (!success) {
        glGetShaderInfoLog(shaderID, 1024, NULL, infoLog);
        printf("Unable to compile shader. \nError log : %s\n", infoLog);
        throw;
    }
}
void ShaderProgram::checkProgramForErrors() {

    int success;
    char infoLog[1024];
    glGetProgramiv(this->shaderProgramID, GL_LINK_STATUS, &success);
    if (!success)
    {
        glGetProgramInfoLog(this->shaderProgramID, 1024, NULL, infoLog);
        printf("Unable to link shaders. \nError log : %s\n", infoLog);
        throw;
    }
}

void ShaderProgram::addShader(std::string shaderPath, uint32_t shaderType) {

    std::stringstream shaderSource;
    std::ifstream source(shaderPath);

    if (source.is_open()) {
        shaderSource << source.rdbuf();
        source.close();
    }
    else {
        printf("Unable to open file <%s>.\n", shaderPath.data());
        source.close();
        throw;
    }

    uint32_t shaderID = glCreateShader(shaderType);

    std::string shaderSourceString = shaderSource.str();
    const char* shaderPtr = shaderSourceString.data();
    glShaderSource(shaderID, 1, &shaderPtr, nullptr);
    glCompileShader(shaderID);

    this->checkShaderForErrors(shaderID);

    this->shaders.push_back(shaderID);
}

void ShaderProgram::createProgram() {

    glDeleteProgram(this->shaderProgramID);

    this->shaderProgramID = glCreateProgram();

    for (int i = 0; i < this->shaders.size(); i++) {
        glAttachShader(this->shaderProgramID, this->shaders[i]);
    }
    glLinkProgram(this->shaderProgramID);

    for (int i = 0; i < this->shaders.size(); i++) {
        glDeleteShader(this->shaders[i]);
    }

    this->shaders.clear();

    this->checkProgramForErrors();
}

void ShaderProgram::use() const {

    glUseProgram(this->shaderProgramID);
}



void ShaderProgram::setInt(const std::string& name, int value) const {

    glUniform1i(glGetUniformLocation(this->shaderProgramID, name.data()), value);
}
void ShaderProgram::setVector2i(const std::string& name, Vector2i vector) const {

    glUniform2i(glGetUniformLocation(this->shaderProgramID, name.data()), vector.x, vector.y);
}
void ShaderProgram::setVector3i(const std::string& name, Vector3i vector) const {

    glUniform3i(glGetUniformLocation(this->shaderProgramID, name.data()), vector.x, vector.y, vector.z);
}
void ShaderProgram::setVector4i(const std::string& name, Vector4i vector) const {

    glUniform4i(glGetUniformLocation(this->shaderProgramID, name.data()), vector.x, vector.y, vector.z, vector.w);
}

void ShaderProgram::setFloat(const std::string& name, float value) const {

    glUniform1f(glGetUniformLocation(this->shaderProgramID, name.data()), value);
}
void ShaderProgram::setVector2f(const std::string& name, Vector2f vector) const {

    glUniform2f(glGetUniformLocation(this->shaderProgramID, name.data()), vector.x, vector.y);
}
void ShaderProgram::setVector3f(const std::string& name, Vector3f vector) const {

    glUniform3f(glGetUniformLocation(this->shaderProgramID, name.data()), vector.x, vector.y, vector.z);
}
void ShaderProgram::setVector4f(const std::string& name, Vector4f vector) const {

    glUniform4f(glGetUniformLocation(this->shaderProgramID, name.data()), vector.x, vector.y, vector.z, vector.w);
}

void ShaderProgram::setIntArray(const std::string& name, int* values, int count) const {

    glUniform1iv(glGetUniformLocation(this->shaderProgramID, name.data()), count, values);
}
void ShaderProgram::setVector2iArray(const std::string& name, Vector2i* values, int count) const {

    glUniform2iv(glGetUniformLocation(this->shaderProgramID, name.data()), count, (int*)values);
}
void ShaderProgram::setVector3iArray(const std::string& name, Vector3i* values, int count) const {

    glUniform3iv(glGetUniformLocation(this->shaderProgramID, name.data()), count, (int*)values);
}
void ShaderProgram::setVector4iArray(const std::string& name, Vector4i* values, int count) const {

    glUniform4iv(glGetUniformLocation(this->shaderProgramID, name.data()), count, (int*)values);
}

void ShaderProgram::setFloatArray(const std::string& name, float* values, int count) const {

    glUniform1fv(glGetUniformLocation(this->shaderProgramID, name.data()), count, values);
}
void ShaderProgram::setVector2fArray(const std::string& name, Vector2f* values, int count) const {

    glUniform2fv(glGetUniformLocation(this->shaderProgramID, name.data()), count, (float*)values);
}
void ShaderProgram::setVector3fArray(const std::string& name, Vector3f* values, int count) const {

    glUniform3fv(glGetUniformLocation(this->shaderProgramID, name.data()), count, (float*)values);
}
void ShaderProgram::setVector4fArray(const std::string& name, Vector4f* values, int count) const {

    glUniform4fv(glGetUniformLocation(this->shaderProgramID, name.data()), count, (float*)values);
}

void ShaderProgram::setMatrix2fArray(const std::string& name, Matrix2f* matrices, int count, bool transpose) const {

    glUniformMatrix2fv(glGetUniformLocation(this->shaderProgramID, name.data()), count, transpose, (float*)matrices);
}
void ShaderProgram::setMatrix3fArray(const std::string& name, Matrix3f* matrices, int count, bool transpose) const {

    glUniformMatrix3fv(glGetUniformLocation(this->shaderProgramID, name.data()), count, transpose, (float*)matrices);
}
void ShaderProgram::setMatrix4fArray(const std::string& name, Matrix4f* matrices, int count, bool transpose) const {
    
    glUniformMatrix4fv(glGetUniformLocation(this->shaderProgramID, name.data()), count, transpose, (float*)matrices);
}



