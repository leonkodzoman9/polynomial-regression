#version 460 core

uniform vec2 point1;
uniform vec2 point2;
  
uniform mat4 orthographicMatrix;

void main() {

    vec2 pos = (gl_VertexID == 0) ? point1 : point2;

    gl_Position = orthographicMatrix * vec4(pos, 0.0, 1.0);
} 






