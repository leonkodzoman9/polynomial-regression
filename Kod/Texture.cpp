#include "Texture.h"



Texture::Texture() {

    this->ID = 0;
    this->target = 0;
}
Texture::Texture(uint32_t target) {

    glGenTextures(1, &this->ID);
    this->target = target;

    this->bind();
}
Texture::Texture(Texture&& rhs) noexcept {

    glDeleteTextures(1, &this->ID);
    this->ID = rhs.ID;
    this->target = rhs.target;
    rhs.ID = 0;
}
Texture& Texture::operator=(Texture&& rhs) noexcept {

    glDeleteTextures(1, &this->ID);
    this->ID = rhs.ID;
    this->target = rhs.target;
    rhs.ID = 0;

    return *this;
}
Texture::~Texture() {

    glDeleteTextures(1, &this->ID);
}

void Texture::setData(uint32_t targetType, uint32_t bufferFormat, int sourceWidth, int sourceHeight,
    uint32_t dataFormat, uint32_t dataColorFormat, void* data) const {

    this->bind();
    glTexImage2D(targetType, 0, bufferFormat, sourceWidth, sourceHeight, 0, dataFormat, dataColorFormat, data);
}

void Texture::setParameter(uint32_t name, uint32_t value) const {

    this->bind();
    glTexParameteri(this->target, name, value);
}

void Texture::generateMipmaps() const {

    this->bind();
    glGenerateMipmap(this->target);
}

void Texture::bind() const {

    glBindTexture(this->target, this->ID);
}
void Texture::unbind() const {

    glBindTexture(this->target, 0);
}

uint32_t Texture::getID() const {

    return this->ID;
}



