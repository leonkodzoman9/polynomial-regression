#version 460 core

uniform vec2 center;
uniform vec2 size;
uniform float angle;

uniform mat4 orthographicMatrix;

void main() {

    mat2 rotmat = mat2(
        vec2(cos(angle), sin(angle)),
        vec2(-sin(angle), cos(angle))
    );

    vec2 poses[4] = {
        vec2(-size.x, -size.y), 
        vec2(size.x, -size.y), 
        vec2(size.x, size.y), 
        vec2(-size.x, size.y)
    };
    
    gl_Position = orthographicMatrix * vec4(center + rotmat * poses[gl_VertexID] / 2, 0.0, 1.0);
} 






