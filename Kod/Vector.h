#pragma once

#include "Includes.h"



static constexpr bool Optimize = true;

template <class Type, int Count>
struct OptimizeFloat {
	static constexpr bool value = std::is_same<Type, float>::value && Count >= 4 && Optimize;
};
template <class Type, int Count>
struct OptimizeDouble {
	static constexpr bool value = std::is_same<Type, double>::value && Count >= 2 && Optimize;
};



// Default vector members
template <class DataType, int Count>
struct VectorMembers {
	std::array<DataType, Count> data;
};

// Members if size is 2
template <class DataType>
struct VectorMembers<DataType, 2> {
	union {
		std::array<DataType, 2> data;
		struct {
			DataType x, y;
		};
	};
};

// Members if size is 3
template <class DataType>
struct VectorMembers<DataType, 3> {
	union {
		std::array<DataType, 3> data;
		struct {
			DataType x, y, z;
		};
	};
};

// Members if size is 4
template <class DataType>
struct VectorMembers<DataType, 4> {
	union {
		std::array<DataType, 4> data;
		struct {
			DataType x, y, z, w;
		};
	};
};



template <class DataType, int Count>
class Vector : public VectorMembers<DataType, Count> {

public:

	// Members are inherited at compile time

	// Constructors

	Vector() {

		for (int i = 0; i < Count; i++) {
			this->data[i] = (DataType)0;
		}
	}
	explicit Vector(DataType val) {

		for (int i = 0; i < Count; i++) {
			this->data[i] = val;
		}
	}

	template <class... ValueType> requires (sizeof...(ValueType) == Count)
		explicit Vector(ValueType... values) {

		int i = 0;
		([&](auto& value) { this->data[i++] = (DataType)value; } (values), ...);
	}

	template <int Count2, class... ValueType> requires ((sizeof...(ValueType) + Count2) == Count && Count2 < Count)
		explicit Vector(Vector<DataType, Count2> vector, ValueType... values) {

		for (int i = 0; i < Count2; i++) {
			this->data[i] = vector.data[i];
		}

		int i = Count2;
		([&](auto& value) { this->data[i++] = (DataType)value; } (values), ...);
	}

	// Convert from any size of same type

	template <int Count2>
	explicit Vector(const Vector<DataType, Count2>& vec) {

		int i = 0;
		for (; i < std::min(Count, Count2); i++) {
			this->data[i] = vec.data[i];
		}
		for (; i < Count; i++) {
			this->data[i] = (DataType)0;
		}
	}

	// Convert from any type of same size

	template <class DataType2>
	Vector(const Vector<DataType2, Count>& vec) {

		for (int i = 0; i < Count; i++) {
			this->data[i] = (DataType)vec.data[i];
		}
	}

	// Access operator

	DataType& operator[](int index) {

		return this->data[index];
	}

	// Operators if normal non-optimized members

	// Addition

	template <class Empty = void> requires (!OptimizeFloat<DataType, Count>::value && !OptimizeDouble<DataType, Count>::value)
		Vector operator+(const Vector& vec) const {

		Vector result;
		for (int i = 0; i < Count; i++) {
			result.data[i] = this->data[i] + vec.data[i];
		}

		return result;
	}
	template <class Empty = void> requires (!OptimizeFloat<DataType, Count>::value && !OptimizeDouble<DataType, Count>::value)
		Vector operator+(DataType val) const {

		Vector result;
		for (int i = 0; i < Count; i++) {
			result.data[i] = this->data[i] + val;
		}

		return result;
	}
	template <class Empty = void> requires (!OptimizeFloat<DataType, Count>::value && !OptimizeDouble<DataType, Count>::value)
		Vector& operator+=(const Vector& vec) {

		for (int i = 0; i < Count; i++) {
			this->data[i] += vec.data[i];
		}

		return *this;
	}
	template <class Empty = void> requires (!OptimizeFloat<DataType, Count>::value && !OptimizeDouble<DataType, Count>::value)
		Vector& operator+=(DataType val) {

		for (int i = 0; i < Count; i++) {
			this->data[i] += val;
		}

		return *this;
	}

	// Subtraction

	template <class Empty = void> requires (!OptimizeFloat<DataType, Count>::value && !OptimizeDouble<DataType, Count>::value)
		Vector operator-(const Vector& vec) const {

		Vector result;
		for (int i = 0; i < Count; i++) {
			result.data[i] = this->data[i] - vec.data[i];
		}

		return result;
	}
	template <class Empty = void> requires (!OptimizeFloat<DataType, Count>::value && !OptimizeDouble<DataType, Count>::value)
		Vector operator-(DataType val) const {

		Vector result;
		for (int i = 0; i < Count; i++) {
			result.data[i] = this->data[i] - val;
		}

		return result;
	}
	template <class Empty = void> requires (!OptimizeFloat<DataType, Count>::value && !OptimizeDouble<DataType, Count>::value)
		Vector& operator-=(const Vector& vec) {

		for (int i = 0; i < Count; i++) {
			this->data[i] -= vec.data[i];
		}

		return *this;
	}
	template <class Empty = void> requires (!OptimizeFloat<DataType, Count>::value && !OptimizeDouble<DataType, Count>::value)
		Vector& operator-=(DataType val) {

		for (int i = 0; i < Count; i++) {
			this->data[i] -= val;
		}

		return *this;
	}

	// Multiplication

	template <class Empty = void> requires (!OptimizeFloat<DataType, Count>::value && !OptimizeDouble<DataType, Count>::value)
		Vector operator*(const Vector& vec) const {

		Vector result;
		for (int i = 0; i < Count; i++) {
			result.data[i] = this->data[i] * vec.data[i];
		}

		return result;
	}
	template <class Empty = void> requires (!OptimizeFloat<DataType, Count>::value && !OptimizeDouble<DataType, Count>::value)
		Vector operator*(DataType val) const {

		Vector result;
		for (int i = 0; i < Count; i++) {
			result.data[i] = this->data[i] * val;
		}

		return result;
	}
	template <class Empty = void> requires (!OptimizeFloat<DataType, Count>::value && !OptimizeDouble<DataType, Count>::value)
		Vector& operator*=(const Vector& vec) {

		for (int i = 0; i < Count; i++) {
			this->data[i] *= vec.data[i];
		}

		return *this;
	}
	template <class Empty = void> requires (!OptimizeFloat<DataType, Count>::value && !OptimizeDouble<DataType, Count>::value)
		Vector& operator*=(DataType val) {

		for (int i = 0; i < Count; i++) {
			this->data[i] *= val;
		}

		return *this;
	}

	// Division

	template <class Empty = void> requires (!OptimizeFloat<DataType, Count>::value && !OptimizeDouble<DataType, Count>::value)
		Vector operator/(const Vector& vec) const {

		Vector result;
		for (int i = 0; i < Count; i++) {
			result.data[i] = this->data[i] / vec.data[i];
		}

		return result;
	}
	template <class Empty = void> requires (!OptimizeFloat<DataType, Count>::value && !OptimizeDouble<DataType, Count>::value)
		Vector operator/(DataType val) const {

		Vector result;
		for (int i = 0; i < Count; i++) {
			result.data[i] = this->data[i] / val;
		}

		return result;
	}
	template <class Empty = void> requires (!OptimizeFloat<DataType, Count>::value && !OptimizeDouble<DataType, Count>::value)
		Vector& operator/=(const Vector& vec) {

		for (int i = 0; i < Count; i++) {
			this->data[i] /= vec.data[i];
		}

		return *this;
	}
	template <class Empty = void> requires (!OptimizeFloat<DataType, Count>::value && !OptimizeDouble<DataType, Count>::value)
		Vector& operator/=(DataType val) {

		for (int i = 0; i < Count; i++) {
			this->data[i] /= val;
		}

		return *this;
	}

	// Operators optimized for floats

	// Addition

	template <class Empty = void> requires (OptimizeFloat<DataType, Count>::value)
		Vector operator+(const Vector& vec) const {

		Vector result;

		for (int i = 0, j = 0; i < Count / 4; i++, j += 4) {
			_mm_storeu_ps(&result.data[j], _mm_add_ps(_mm_loadu_ps(&this->data[j]), _mm_loadu_ps(&vec.data[j])));
		}
		for (int i = (Count / 4) * 4; i < Count; i++) {
			result.data[i] = this->data[i] + vec.data[i];
		}

		return result;
	}
	template <class Empty = void> requires (OptimizeFloat<DataType, Count>::value)
		Vector operator+(DataType val) const {

		__m128 _val = _mm_set1_ps(val);

		Vector result;

		for (int i = 0, j = 0; i < Count / 4; i++, j += 4) {
			_mm_storeu_ps(&result.data[j], _mm_add_ps(_mm_loadu_ps(&this->data[j]), _val));
		}
		for (int i = (Count / 4) * 4; i < Count; i++) {
			result.data[i] = this->data[i] + val;
		}

		return result;
	}
	template <class Empty = void> requires (OptimizeFloat<DataType, Count>::value)
		Vector& operator+=(const Vector& vec) {

		for (int i = 0, j = 0; i < Count / 4; i++, j += 4) {
			_mm_storeu_ps(&this->data[j], _mm_add_ps(_mm_loadu_ps(&this->data[j]), _mm_loadu_ps(&vec.data[j])));
		}
		for (int i = (Count / 4) * 4; i < Count; i++) {
			this->data[i] += vec.data[i];
		}

		return *this;
	}
	template <class Empty = void> requires (OptimizeFloat<DataType, Count>::value)
		Vector& operator+=(DataType val) {

		__m128 _val = _mm_set1_ps(val);

		for (int i = 0, j = 0; i < Count / 4; i++, j += 4) {
			_mm_storeu_ps(&this->data[j], _mm_add_ps(_mm_loadu_ps(&this->data[j]), _val));
		}
		for (int i = (Count / 4) * 4; i < Count; i++) {
			this->data[i] += val;
		}

		return *this;
	}

	// Subtraction

	template <class Empty = void> requires (OptimizeFloat<DataType, Count>::value)
		Vector operator-(const Vector& vec) const {

		Vector result;

		for (int i = 0, j = 0; i < Count / 4; i++, j += 4) {
			_mm_storeu_ps(&result.data[j * 4], _mm_sub_ps(_mm_loadu_ps(&this->data[j]), _mm_loadu_ps(&vec.data[j])));
		}
		for (int i = (Count / 4) * 4; i < Count; i++) {
			result.data[i] = this->data[i] - vec.data[i];
		}

		return result;
	}
	template <class Empty = void> requires (OptimizeFloat<DataType, Count>::value)
		Vector operator-(DataType val) const {

		__m128 _val = _mm_set1_ps(val);

		Vector result;

		for (int i = 0, j = 0; i < Count / 4; i++, j += 4) {
			_mm_storeu_ps(&result.data[j], _mm_sub_ps(_mm_loadu_ps(&this->data[j]), _val));
		}
		for (int i = (Count / 4) * 4; i < Count; i++) {
			result.data[i] = this->data[i] - val;
		}

		return result;
	}
	template <class Empty = void> requires (OptimizeFloat<DataType, Count>::value)
		Vector& operator-=(const Vector& vec) {

		for (int i = 0, j = 0; i < Count / 4; i++, j += 4) {
			_mm_storeu_ps(&this->data[j], _mm_sub_ps(_mm_loadu_ps(&this->data[j]), _mm_loadu_ps(&vec.data[j])));
		}
		for (int i = (Count / 4) * 4; i < Count; i++) {
			this->data[i] -= vec.data[i];
		}

		return *this;
	}
	template <class Empty = void> requires (OptimizeFloat<DataType, Count>::value)
		Vector& operator-=(DataType val) {

		__m128 _val = _mm_set1_ps(val);

		for (int i = 0, j = 0; i < Count / 4; i++, j += 4) {
			_mm_storeu_ps(&this->data[j], _mm_sub_ps(_mm_loadu_ps(&this->data[j]), _val));
		}
		for (int i = (Count / 4) * 4; i < Count; i++) {
			this->data[i] -= val;
		}

		return *this;
	}

	// Multiplication

	template <class Empty = void> requires (OptimizeFloat<DataType, Count>::value)
		Vector operator*(const Vector& vec) const {

		Vector result;

		for (int i = 0, j = 0; i < Count / 4; i++, j += 4) {
			_mm_storeu_ps(&result.data[j], _mm_mul_ps(_mm_loadu_ps(&this->data[j]), _mm_loadu_ps(&vec.data[j])));
		}
		for (int i = (Count / 4) * 4; i < Count; i++) {
			result.data[i] = this->data[i] * vec.data[i];
		}

		return result;
	}
	template <class Empty = void> requires (OptimizeFloat<DataType, Count>::value)
		Vector operator*(DataType val) const {

		__m128 _val = _mm_set1_ps(val);

		Vector result;

		for (int i = 0, j = 0; i < Count / 4; i++, j += 4) {
			_mm_storeu_ps(&result.data[j], _mm_mul_ps(_mm_loadu_ps(&this->data[j]), _val));
		}
		for (int i = (Count / 4) * 4; i < Count; i++) {
			result.data[i] = this->data[i] * val;
		}

		return result;
	}
	template <class Empty = void> requires (OptimizeFloat<DataType, Count>::value)
		Vector& operator*=(const Vector& vec) {

		for (int i = 0, j = 0; i < Count / 4; i++, j += 4) {
			_mm_storeu_ps(&this->data[j], _mm_mul_ps(_mm_loadu_ps(&this->data[j]), _mm_loadu_ps(&vec.data[j])));
		}
		for (int i = (Count / 4) * 4; i < Count; i++) {
			this->data[i] *= vec.data[i];
		}

		return *this;
	}
	template <class Empty = void> requires (OptimizeFloat<DataType, Count>::value)
		Vector& operator*=(DataType val) {

		__m128 _val = _mm_set1_ps(val);

		for (int i = 0, j = 0; i < Count / 4; i++, j += 4) {
			_mm_storeu_ps(&this->data[j], _mm_mul_ps(_mm_loadu_ps(&this->data[j]), _val));
		}
		for (int i = (Count / 4) * 4; i < Count; i++) {
			this->data[i] *= val;
		}

		return *this;
	}

	// Division

	template <class Empty = void> requires (OptimizeFloat<DataType, Count>::value)
		Vector operator/(const Vector& vec) const {

		Vector result;

		for (int i = 0, j = 0; i < Count / 4; i++, j += 4) {
			_mm_storeu_ps(&result.data[j], _mm_div_ps(_mm_loadu_ps(&this->data[j]), _mm_loadu_ps(&vec.data[j])));
		}
		for (int i = (Count / 4) * 4; i < Count; i++) {
			result.data[i] = this->data[i] / vec.data[i];
		}

		return result;
	}
	template <class Empty = void> requires (OptimizeFloat<DataType, Count>::value)
		Vector operator/(DataType val) const {

		__m128 _val = _mm_set1_ps(val);

		Vector result;

		for (int i = 0, j = 0; i < Count / 4; i++, j += 4) {
			_mm_storeu_ps(&result.data[j], _mm_div_ps(_mm_loadu_ps(&this->data[j]), _val));
		}
		for (int i = (Count / 4) * 4; i < Count; i++) {
			result.data[i] = this->data[i] / val;
		}

		return result;
	}
	template <class Empty = void> requires (OptimizeFloat<DataType, Count>::value)
		Vector& operator/=(const Vector& vec) {

		for (int i = 0, j = 0; i < Count / 4; i++, j += 4) {
			_mm_storeu_ps(&this->data[j], _mm_div_ps(_mm_loadu_ps(&this->data[j]), _mm_loadu_ps(&vec.data[j])));
		}
		for (int i = (Count / 4) * 4; i < Count; i++) {
			this->data[i] /= vec.data[i];
		}

		return *this;
	}
	template <class Empty = void> requires (OptimizeFloat<DataType, Count>::value)
		Vector& operator/=(DataType val) {

		__m128 _val = _mm_set1_ps(val);

		for (int i = 0, j = 0; i < Count / 4; i++, j += 4) {
			_mm_storeu_ps(&this->data[j], _mm_div_ps(_mm_loadu_ps(&this->data[j]), _val));
		}
		for (int i = (Count / 4) * 4; i < Count; i++) {
			this->data[i] /= val;
		}

		return *this;
	}

	// Operators optimized for doubles

	// Addition

	template <class Empty = void> requires (OptimizeDouble<DataType, Count>::value)
		Vector operator+(const Vector& vec) const {

		Vector result;

		for (int i = 0, j = 0; i < Count / 2; i++, j += 2) {
			_mm_storeu_pd(&result.data[j], _mm_add_pd(_mm_loadu_pd(&this->data[j]), _mm_loadu_pd(&vec.data[j])));
		}
		for (int i = (Count / 2) * 2; i < Count; i++) {
			result.data[i] = this->data[i] + vec.data[i];
		}

		return result;
	}
	template <class Empty = void> requires (OptimizeDouble<DataType, Count>::value)
		Vector operator+(DataType val) const {

		__m128d _val = _mm_set1_pd(val);

		Vector result;

		for (int i = 0, j = 0; i < Count / 2; i++, j += 2) {
			_mm_storeu_pd(&result.data[j], _mm_add_pd(_mm_loadu_pd(&this->data[j]), _val));
		}
		for (int i = (Count / 2) * 2; i < Count; i++) {
			result.data[i] = this->data[i] + val;
		}

		return result;
	}
	template <class Empty = void> requires (OptimizeDouble<DataType, Count>::value)
		Vector& operator+=(const Vector& vec) {

		for (int i = 0, j = 0; i < Count / 2; i++, j += 2) {
			_mm_storeu_pd(&this->data[j], _mm_add_pd(_mm_loadu_pd(&this->data[j]), _mm_loadu_pd(&vec.data[j])));
		}
		for (int i = (Count / 2) * 2; i < Count; i++) {
			this->data[i] += vec.data[i];
		}

		return *this;
	}
	template <class Empty = void> requires (OptimizeDouble<DataType, Count>::value)
		Vector& operator+=(DataType val) {

		__m128d _val = _mm_set1_pd(val);

		for (int i = 0, j = 0; i < Count / 2; i++, j += 2) {
			_mm_storeu_pd(&this->data[j], _mm_add_pd(_mm_loadu_pd(&this->data[j]), _val));
		}
		for (int i = (Count / 2) * 2; i < Count; i++) {
			this->data[i] += val;
		}

		return *this;
	}

	// Subtraction

	template <class Empty = void> requires (OptimizeDouble<DataType, Count>::value)
		Vector operator-(const Vector& vec) const {

		Vector result;

		for (int i = 0, j = 0; i < Count / 2; i++, j += 2) {
			_mm_storeu_pd(&result.data[j * 2], _mm_sub_pd(_mm_loadu_pd(&this->data[j]), _mm_loadu_pd(&vec.data[j])));
		}
		for (int i = (Count / 2) * 2; i < Count; i++) {
			result.data[i] = this->data[i] - vec.data[i];
		}

		return result;
	}
	template <class Empty = void> requires (OptimizeDouble<DataType, Count>::value)
		Vector operator-(DataType val) const {

		__m128d _val = _mm_set1_pd(val);

		Vector result;

		for (int i = 0, j = 0; i < Count / 2; i++, j += 2) {
			_mm_storeu_pd(&result.data[j], _mm_sub_pd(_mm_loadu_pd(&this->data[j]), _val));
		}
		for (int i = (Count / 2) * 2; i < Count; i++) {
			result.data[i] = this->data[i] - val;
		}

		return result;
	}
	template <class Empty = void> requires (OptimizeDouble<DataType, Count>::value)
		Vector& operator-=(const Vector& vec) {

		for (int i = 0, j = 0; i < Count / 2; i++, j += 2) {
			_mm_storeu_pd(&this->data[j], _mm_sub_pd(_mm_loadu_pd(&this->data[j]), _mm_loadu_pd(&vec.data[j])));
		}
		for (int i = (Count / 2) * 2; i < Count; i++) {
			this->data[i] -= vec.data[i];
		}

		return *this;
	}
	template <class Empty = void> requires (OptimizeDouble<DataType, Count>::value)
		Vector& operator-=(DataType val) {

		__m128d _val = _mm_set1_pd(val);

		for (int i = 0, j = 0; i < Count / 2; i++, j += 2) {
			_mm_storeu_pd(&this->data[j], _mm_sub_pd(_mm_loadu_pd(&this->data[j]), _val));
		}
		for (int i = (Count / 2) * 2; i < Count; i++) {
			this->data[i] -= val;
		}

		return *this;
	}

	// Multiplication

	template <class Empty = void> requires (OptimizeDouble<DataType, Count>::value)
		Vector operator*(const Vector& vec) const {

		Vector result;

		for (int i = 0, j = 0; i < Count / 2; i++, j += 2) {
			_mm_storeu_pd(&result.data[j], _mm_mul_pd(_mm_loadu_pd(&this->data[j]), _mm_loadu_pd(&vec.data[j])));
		}
		for (int i = (Count / 2) * 2; i < Count; i++) {
			result.data[i] = this->data[i] * vec.data[i];
		}

		return result;
	}
	template <class Empty = void> requires (OptimizeDouble<DataType, Count>::value)
		Vector operator*(DataType val) const {

		__m128d _val = _mm_set1_pd(val);

		Vector result;

		for (int i = 0, j = 0; i < Count / 2; i++, j += 2) {
			_mm_storeu_pd(&result.data[j], _mm_mul_pd(_mm_loadu_pd(&this->data[j]), _val));
		}
		for (int i = (Count / 2) * 2; i < Count; i++) {
			result.data[i] = this->data[i] * val;
		}

		return result;
	}
	template <class Empty = void> requires (OptimizeDouble<DataType, Count>::value)
		Vector& operator*=(const Vector& vec) {

		for (int i = 0, j = 0; i < Count / 2; i++, j += 2) {
			_mm_storeu_pd(&this->data[j], _mm_mul_pd(_mm_loadu_pd(&this->data[j]), _mm_loadu_pd(&vec.data[j])));
		}
		for (int i = (Count / 2) * 2; i < Count; i++) {
			this->data[i] *= vec.data[i];
		}

		return *this;
	}
	template <class Empty = void> requires (OptimizeDouble<DataType, Count>::value)
		Vector& operator*=(DataType val) {

		__m128d _val = _mm_set1_pd(val);

		for (int i = 0, j = 0; i < Count / 2; i++, j += 2) {
			_mm_storeu_pd(&this->data[j], _mm_mul_pd(_mm_loadu_pd(&this->data[j]), _val));
		}
		for (int i = (Count / 2) * 2; i < Count; i++) {
			this->data[i] *= val;
		}

		return *this;
	}

	// Division

	template <class Empty = void> requires (OptimizeDouble<DataType, Count>::value)
		Vector operator/(const Vector& vec) const {

		Vector result;

		for (int i = 0, j = 0; i < Count / 2; i++, j += 2) {
			_mm_storeu_pd(&result.data[j], _mm_div_pd(_mm_loadu_pd(&this->data[j]), _mm_loadu_pd(&vec.data[j])));
		}
		for (int i = (Count / 2) * 2; i < Count; i++) {
			result.data[i] = this->data[i] / vec.data[i];
		}

		return result;
	}
	template <class Empty = void> requires (OptimizeDouble<DataType, Count>::value)
		Vector operator/(DataType val) const {

		__m128d _val = _mm_set1_pd(val);

		Vector result;

		for (int i = 0, j = 0; i < Count / 2; i++, j += 2) {
			_mm_storeu_pd(&result.data[j], _mm_div_pd(_mm_loadu_pd(&this->data[j]), _val));
		}
		for (int i = (Count / 2) * 2; i < Count; i++) {
			result.data[i] = this->data[i] / val;
		}

		return result;
	}
	template <class Empty = void> requires (OptimizeDouble<DataType, Count>::value)
		Vector& operator/=(const Vector& vec) {

		for (int i = 0, j = 0; i < Count / 2; i++, j += 2) {
			_mm_storeu_pd(&this->data[j], _mm_div_pd(_mm_loadu_pd(&this->data[j]), _mm_loadu_pd(&vec.data[j])));
		}
		for (int i = (Count / 2) * 2; i < Count; i++) {
			this->data[i] /= vec.data[i];
		}

		return *this;
	}
	template <class Empty = void> requires (OptimizeDouble<DataType, Count>::value)
		Vector& operator/=(DataType val) {

		__m128d _val = _mm_set1_pd(val);

		for (int i = 0, j = 0; i < Count / 2; i++, j += 2) {
			_mm_storeu_pd(&this->data[j], _mm_div_pd(_mm_loadu_pd(&this->data[j]), _val));
		}
		for (int i = (Count / 2) * 2; i < Count; i++) {
			this->data[i] /= val;
		}

		return *this;
	}

	// Assignment, from any type of same size

	template <class DataType2>
	Vector<DataType, Count>& operator=(const Vector<DataType2, Count>& vec) {

		for (int i = 0; i < Count; i++) {
			this->data[i] = (DataType)vec.data[i];
		}

		return *this;
	}

	// Comparison

	bool operator==(const Vector& vec) const {

		int equals = 0;
		for (int i = 0; i < Count; i++) {
			equals += this->data[i] == vec.data[i] ? 1 : 0;
		}

		return equals == Count;
	}
	bool operator!=(const Vector& vec) const {

		return !(*this == vec);
	}

	// Negate

	Vector operator - () const {

		return *this * -1;
	}

	// Properties

	DataType magnitude() const {

		return (DataType)std::sqrt(this->magnitude2());
	}
	DataType magnitude2() const {

		DataType sum = 0;
		for (int i = 0; i < Count; i++) {
			sum += this->data[i] * this->data[i];
		}

		return sum;
	}

	// 2D Vector property

	template <class Empty = void> requires (Count == 2)
		DataType angle() const {

		return (DataType)std::atan2(this->y, this->x);
	}

	// 3D Vector properties

	template <class Empty = void> requires (Count == 3)
		DataType yaw() const {

		return (DataType)std::atan2(this->z, this->x);
	}
	template <class Empty = void> requires (Count == 3)
		DataType pitch() const {

		return (DataType)std::atan2(this->y, (DataType)std::sqrt(this->x * this->x + this->z * this->z));
	}

	// 3D Vector directions

	template <class Empty = void> requires (Count == 3)
		static Vector Up() {

		return Vector(0, 1, 0);
	}
	template <class Empty = void> requires (Count == 3)
		static Vector Down() {

		return Vector(0, -1, 0);
	}
	template <class Empty = void> requires (Count == 3)
		static Vector Left() {

		return Vector(-1, 0, 0);
	}
	template <class Empty = void> requires (Count == 3)
		static Vector Right() {

		return Vector(1, 0, 0);
	}
	template <class Empty = void> requires (Count == 3)
		static Vector Front() {

		return Vector(0, 0, 1);
	}
	template <class Empty = void> requires (Count == 3)
		static Vector Back() {

		return Vector(0, 0, -1);
	}

	static int64_t Size() {
		return Count;
	}
};



typedef class Vector<int, 2> Vector2i;
typedef class Vector<int, 3> Vector3i;
typedef class Vector<int, 4> Vector4i;

typedef class Vector<int64_t, 2> Vector2l;
typedef class Vector<int64_t, 3> Vector3l;
typedef class Vector<int64_t, 4> Vector4l;

typedef class Vector<float, 2> Vector2f;
typedef class Vector<float, 3> Vector3f;
typedef class Vector<float, 4> Vector4f;

typedef class Vector<double, 2> Vector2d;
typedef class Vector<double, 3> Vector3d;
typedef class Vector<double, 4> Vector4d;



namespace VectorMath {

	template <class DataType, int Count>
	Vector<DataType, Count> Normalize(const Vector<DataType, Count>& vec) {

		return vec / vec.magnitude();
	}

	template <class DataType, int Count>
	DataType Dot(const Vector<DataType, Count>& vec1, const Vector<DataType, Count>& vec2) {

		DataType sum = 0;
		for (int i = 0; i < Count; i++) {
			sum += vec1.data[i] * vec2.data[i];
		}

		return sum;
	}

	template <class DataType>
	Vector<DataType, 2> FromPolar(DataType length, DataType angle) {

		Vector<DataType, 2> result;
		result.data[0] = length * (DataType)std::cos(angle);
		result.data[1] = length * (DataType)std::sin(angle);

		return result;
	}

	template <class DataType>
	Vector<DataType, 3> FromPolar(DataType length, DataType yaw, DataType pitch) {

		Vector<DataType, 3> result;
		result.data[0] = (DataType)std::cos(yaw) * (DataType)std::sin(pitch);
		result.data[1] = (DataType)std::sin(pitch);
		result.data[2] = (DataType)std::sin(yaw) * (DataType)std::sin(pitch);

		return result;
	}

	template <class DataType>
	Vector<DataType, 3> Cross(const Vector<DataType, 3>& vec1, const Vector<DataType, 3>& vec2) {

		Vector<DataType, 3> result;
		result.data[0] = vec1.y * vec2.z - vec1.z * vec2.y;
		result.data[1] = -(vec1.x * vec2.z - vec1.z * vec2.x);
		result.data[2] = vec1.x * vec2.y - vec1.y * vec2.x;

		return result;
	}

	template <class DataType>
	Vector<DataType, 3> Rotate(const Vector<DataType, 3>& vec, const Vector<DataType, 3>& axis, DataType angle) {

		Vector<DataType, 3> part1 = vec * (DataType)std::cos(angle);
		Vector<DataType, 3> part2 = VectorMath::Cross<DataType>(axis, vec) * (DataType)std::sin(angle);
		Vector<DataType, 3> part3 = axis * VectorMath::Dot(axis, vec) * ((DataType)1.0 - (DataType)std::cos(angle));

		return part1 + part2 + part3;
	}

	template <class DataType, int Count>
	Vector<DataType, Count> Apply(const Vector<DataType, Count>& vec, DataType(*function)(DataType value)) {

		Vector<DataType, Count> result;
		for (int i = 0; i < Count; i++) {
			result.data[i] = function(vec.data[i]);
		}

		return result;
	}

	template <class DataType, int Count>
	Vector<DataType, Count> Apply(const Vector<DataType, Count>& vec, auto&& function) {

		Vector<DataType, Count> result;
		for (int i = 0; i < Count; i++) {
			result.data[i] = function(vec.data[i]);
		}

		return result;
	}

	template <class DataType, int Count>
	Vector<DataType, Count> Apply(const Vector<DataType, Count>& vec1, const Vector<DataType, Count>& vec2, DataType(*function)(DataType value1, DataType value2)) {

		Vector<DataType, Count> result;
		for (int i = 0; i < Count; i++) {
			result.data[i] = function(vec1.data[i], vec2.data[i]);
		}

		return result;
	}

	template <class DataType, int Count>
	Vector<DataType, Count> Apply(const Vector<DataType, Count>& vec1, const Vector<DataType, Count>& vec2, auto&& function) {

		Vector<DataType, Count> result;
		for (int i = 0; i < Count; i++) {
			result.data[i] = function(vec1.data[i], vec2.data[i]);
		}

		return result;
	}

	template <class DataType, int Count>
	void ApplyTo(Vector<DataType, Count>& vec, DataType(*function)(DataType value)) {

		for (int i = 0; i < Count; i++) {
			vec.data[i] = function(vec.data[i]);
		}
	}

	template <class DataType, int Count>
	void ApplyTo(Vector<DataType, Count>& vec, auto&& function) {

		for (int i = 0; i < Count; i++) {
			vec.data[i] = function(vec.data[i]);
		}
	}

	template <class DataType, int Count>
	DataType Reduce(const Vector<DataType, Count>& vec, DataType(*function)(DataType value1, DataType value2)) {

		DataType result = function(vec.data[0], vec.data[1]);
		for (int i = 2; i < Count; i++) {
			result = function(result, vec.data[i]);
		}

		return result;
	}

	template <class DataType, int Count>
	DataType Reduce(const Vector<DataType, Count>& vec, auto&& function) {

		DataType result = function(vec.data[0], vec.data[1]);
		for (int i = 2; i < Count; i++) {
			result = function(result, vec.data[i]);
		}

		return result;
	}

	template <class DataType, int Count>
	Vector<DataType, Count> Interpolate(const Vector<DataType, Count>& vec1, const Vector<DataType, Count>& vec2, float amount) {

		Vector<DataType, Count> diff = vec2 - vec1;

		return vec1 + diff * amount;
	}
}





