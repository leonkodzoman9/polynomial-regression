#pragma once

#include "Includes.h"

#include "ShaderProgram.h"
#include "Texture.h"
#include "Vector.h"



struct Symbol {

    Texture texture;
    Vector2f size;
    Vector2f offset;
    float characterWidth;
};



class Font {

private:

    // Height of font and all 256 ascii symbols

    int height;
    Symbol symbols[256];

public:

    Font();
    // Create font based on name and height in pixels
    Font(std::string fontName, int height = 64);
    
    Font(const Font& rhs) = delete;
    Font& operator=(const Font& rhs) = delete;
    Font(Font&& rhs) noexcept;
    Font& operator=(Font&& rhs) noexcept;
    
    // Get height of font
    int getHeight() const;
    // Get symbol at index
    Symbol& getSymbol(uint8_t index);
};



