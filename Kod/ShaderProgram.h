#pragma once

#include "Includes.h"

#include "Vector.h"
#include "Matrix.h"



class ShaderProgram {

private:

    uint32_t shaderProgramID;
    std::vector<uint32_t> shaders;

    // Check individual shaders for errors
    void checkShaderForErrors(uint32_t shaderID);
    // Check the whole shader program for errors
    void checkProgramForErrors();

public:

    ShaderProgram();
    ~ShaderProgram();

    // One cannot copy or move shader programs
    ShaderProgram(const ShaderProgram& rhs) = delete;
    ShaderProgram& operator=(const ShaderProgram& rhs) = delete;
    ShaderProgram(ShaderProgram&& rhs) = delete;
    ShaderProgram& operator=(ShaderProgram&& rhs) = delete;

    // Add new shader and its type
    void addShader(std::string shaderPath, uint32_t shaderType);
    // Finalize shader program
    void createProgram();

    // Use the current shader program
    void use() const;

    // Integer uniforms

    void setInt(const std::string& name, int value) const;
    void setVector2i(const std::string& name, Vector2i vector) const;
    void setVector3i(const std::string& name, Vector3i vector) const;
    void setVector4i(const std::string& name, Vector4i vector) const;

    void setIntArray(const std::string& name, int* values, int count) const;
    void setVector2iArray(const std::string& name, Vector2i* vector, int count) const;
    void setVector3iArray(const std::string& name, Vector3i* vector, int count) const;
    void setVector4iArray(const std::string& name, Vector4i* vector, int count) const;

    // Float uniforms

    void setFloat(const std::string& name, float value) const;
    void setVector2f(const std::string& name, Vector2f vector) const;
    void setVector3f(const std::string& name, Vector3f vector) const;
    void setVector4f(const std::string& name, Vector4f vector) const;

    void setFloatArray(const std::string& name, float* value, int count) const;
    void setVector2fArray(const std::string& name, Vector2f* vector, int count) const;
    void setVector3fArray(const std::string& name, Vector3f* vector, int count) const;
    void setVector4fArray(const std::string& name, Vector4f* vector, int count) const;

    // Matrix uniforms

    void setMatrix2fArray(const std::string& name, Matrix2f* matrices, int count, bool transpose = false) const;
    void setMatrix3fArray(const std::string& name, Matrix3f* matrices, int count, bool transpose = false) const;
    void setMatrix4fArray(const std::string& name, Matrix4f* matrices, int count, bool transpose = false) const;
};


