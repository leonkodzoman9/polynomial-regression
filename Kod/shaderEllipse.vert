#version 460 core

uniform vec2 point;
uniform vec2 radii;
uniform float angle;
uniform int divisions;

uniform mat4 orthographicMatrix;

const float pi = 3.1415926535;

void main() {

    mat2 rotmat = mat2(
        vec2(cos(angle), sin(angle)),
        vec2(-sin(angle), cos(angle))
    );

    float pointAngle = 2 * pi * gl_VertexID / divisions;

    vec2 pos = radii * vec2(cos(pointAngle), sin(pointAngle));

    gl_Position = orthographicMatrix * vec4(point + rotmat * pos, 0.0, 1.0);
} 






